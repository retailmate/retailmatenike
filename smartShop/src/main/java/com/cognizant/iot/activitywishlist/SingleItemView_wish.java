package com.cognizant.iot.activitywishlist;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.retailmate.R;

public class SingleItemView_wish extends Activity {
	// Declare Variables
	String prodid;
	String prodname;
	String prodprice;
	String flag;
	String beacon;
	String desc;
	String position;
	ImageLoader_wish imageLoader = new ImageLoader_wish(this);
	ProgressDialog mProgressDialog;
	JSONObject jsonobject;
	JSONArray jsonarray;
	ArrayList<HashMap<String, String>> arraylist;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from singleitemview.xml
		setContentView(R.layout.singleitemview_wish);

		Intent i = getIntent();
		// Get the result of prodid
		prodid = i.getStringExtra("prodid");
		// Get the result of prodname
		prodname = i.getStringExtra("prodname");
		// Get the result of prodprice
		prodprice = i.getStringExtra("prodprice");
		// Get the result of flag
		flag = i.getStringExtra("flag");
		desc = i.getStringExtra("desc");
		beacon = i.getStringExtra("beacon");

		// Locate the TextViews in singleitemview.xml
		TextView txtprodid = (TextView) findViewById(R.id.prodid2_wish);
		TextView txtprodname = (TextView) findViewById(R.id.prodname2_wish);
		TextView txtprodprice = (TextView) findViewById(R.id.prodprice2_wish);
		Button btndeletefromwshlst = (Button) findViewById(R.id.removefromwishlist_btn);
		TextView txtdesc = (TextView) findViewById(R.id.prod_desc_wish);
		TextView txtbeacon = (TextView) findViewById(R.id.beaconid_wish);

		// Locate the ImageView in singleitemview.xml
		ImageView imgflag = (ImageView) findViewById(R.id.flag2_wish);

		// Set results to the TextViews
		txtprodid.setText(prodid);
		txtprodname.setText(prodname);
		txtprodprice.setText(prodprice);
		txtdesc.setText(desc);
		txtbeacon.setText(beacon);

		// Capture position and set results to the ImageView
		// Passes flag images URL into ImageLoader.class
		imageLoader.DisplayImage(flag, imgflag);

		btndeletefromwshlst.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DownloadJSONfordelete().execute();
				Intent hh = new Intent(SingleItemView_wish.this,
						CatalogActivity.class);
				startActivity(hh);

			}
		});

	}

	// DownloadJSON AsyncTask
	private class DownloadJSONfordelete extends AsyncTask<Void, Void, Void> {
		String str4 = null, str3 = null, str5 = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(SingleItemView_wish.this);
			// Set progressdialog title
			mProgressDialog.setTitle("Deleting");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			/*
			 * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

			// Create an array
			arraylist = new ArrayList<HashMap<String, String>>();
			// Retrieve JSON Objects from the given URL address
			jsonobject = JSONfunctions_wish_deleteproduct.getJSONfromURL(
					CatalogActivity.urlPart
							+ "/api/WishList/DeleteWishlistProducts", prodid);

			try {

				JSONObject obj1 = new JSONObject(jsonobject.getString("header"));

				String message = obj1.getString("message");
				// Toast.makeText(getApplicationContext(),"Response is\n"
				// +obj1.toString(),Toast.LENGTH_LONG).show();

				if (message.equalsIgnoreCase("Request processed successfully")) {

					// Toast.makeText(getApplicationContext(),"Request processed successfully",Toast.LENGTH_LONG).show();

					String status = obj1.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray obj3 = (JSONArray) jsonobject
								.get("cusdetails");

						try {
							str3 = (String) obj3.getJSONObject(0).get("prodid");
							// str4=(String)
							// obj3.getJSONObject(0).get("Products added");
							/*
							 * Toast.makeText(getApplicationContext(),
							 * "Request processed successfully\nProduct "
							 * +str3,Toast.LENGTH_LONG).show();
							 */

						}

						catch (JSONException j) {

							j.printStackTrace();
							/*
							 * Toast.makeText(getApplicationContext(),
							 * "Some error in reading JSON"
							 * ,Toast.LENGTH_LONG).show();
							 */
						}
					}

				}
			} catch (JSONException e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

			// Toast.makeText(getApplicationContext(),jsonobject.toString() ,
			// Toast.LENGTH_LONG).show();
			// Intent i=new
			// Intent(SingleItemView_wish.class,MainActivity_wish.class);
			Toast.makeText(getApplicationContext(),
					"Request processed successfully\nProduct " + str3,
					Toast.LENGTH_LONG).show();

			mProgressDialog.dismiss();
		}
	}
}
