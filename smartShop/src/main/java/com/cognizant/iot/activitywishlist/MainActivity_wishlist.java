package com.cognizant.iot.activitywishlist;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.adapter.ListViewAdapter_wish_full;
import com.cognizant.retailmate.R;

public class MainActivity_wishlist extends Activity {
	// Declare Variables
	JSONObject jsonobject, jsonobject1;
	JSONArray jsonarray;
	ListView listview;
	ListViewAdapter_wish_full adapter;
	ProgressDialog mProgressDialog;
	ArrayList<HashMap<String, String>> arraylist;
	public static String PRODID = "prodid";
	public static String CATEGORY = "category";
	public static String UNIT = "unit";
	public static String PRODNAME = "prodname";
	public static String PRODPRICE = "prodprice";
	public static String FLAG = "flag";
	Boolean arrayempty = false;
	public static String DESC = "desc";
	public static String BEACON = "beacon";
	public static String OFFER = "offer";
	public static String imei = "000000000000789";
	Intent wishlist_intent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from listview_main.xml
		setContentView(R.layout.listview_mainwishlist);
		// Execute DownloadJSON AsyncTask
		new DownloadJSON().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.share_wish_btn:
			wishlist_intent = new Intent(this, Share_Wishlist.class);
			startActivity(wishlist_intent);

			break;

		default:
			break;
		}
		return true;

	}

	// DownloadJSON AsyncTask
	private class DownloadJSON extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(MainActivity_wishlist.this);
			// Set progressdialog title
			mProgressDialog.setTitle("My WishList");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {

			/*
			 * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

			// Create an array
			arraylist = new ArrayList<HashMap<String, String>>();
			// Retrieve JSON Objects from the given URL address
			jsonobject1 = JSONfunctions_wish
					.getJSONfromURL(CatalogActivity.urlPart
							+ "/api/WishList/GetWishListDetails");

			try {
				// Locate the array name in JSON
				jsonarray = jsonobject1.getJSONArray("cusdetails");

				if (jsonarray.length() == 0)
					arrayempty = true;
				else {
					for (int i = 0; i < jsonarray.length(); i++) {
						HashMap<String, String> map = new HashMap<String, String>();
						jsonobject = jsonarray.getJSONObject(i);
						// Retrive JSON Objects
						map.put("prodid", jsonobject.getString("prodid"));
						map.put("category", jsonobject.getString("Category"));
						map.put("unit", jsonobject.getString("Unit"));
						map.put("prodname", jsonobject.getString("asset"));
						// map.put("prodprice", "$" +
						// jsonobject.getString("Cost"));
						map.put("prodprice",
								"$" + jsonobject.getString("Cost"));
						map.put("flag", jsonobject.getString("Thumb"));
						map.put("desc", jsonobject.getString("Desc"));
						map.put("beacon", jsonobject.getString("Beacon"));
						map.put("offer", jsonobject.getString("Offer"));
						// Set the JSON Objects into the array
						arraylist.add(map);
					}
				}
			} catch (JSONException e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

			// Toast.makeText(getApplicationContext(),jsonobject1.toString() ,
			// Toast.LENGTH_LONG).show();

			if (arrayempty == true)
				Toast.makeText(getApplicationContext(),
						"Your wishlist is empty", Toast.LENGTH_LONG).show();

			// Locate the listview in listview_main.xml
			listview = (ListView) findViewById(R.id.listview_wish);
			// Pass the results into ListViewAdapter.java
			adapter = new ListViewAdapter_wish_full(MainActivity_wishlist.this,
					arraylist);
			// Set the adapter to the ListView
			adapter.notifyDataSetChanged();
			listview.setAdapter(adapter);
			// Close the progressdialog
			mProgressDialog.dismiss();
		}
	}
}