package com.cognizant.iot.geolocation;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;

public class ProximityReciever extends BroadcastReceiver {
	Context context;

	@Override
	public void onReceive(Context context, Intent intent) {

		this.context = context;

		// Key for determining whether user is leaving or entering
		String key = LocationManager.KEY_PROXIMITY_ENTERING;

		// Gives whether the user is entering or leaving in boolean form
		boolean state = intent.getBooleanExtra(key, false);

		if (state) {
			// Call the Notification Service or anything else that you would
			// like to do here
			Log.i("MyTag", "Welcome to RetailMate Area");
			// Toast.makeText(context, "Welcome to RetailMate Area",
			// Toast.LENGTH_SHORT).show();

			sendNotification();

			// context.unregisterReceiver(this);

			// Intent i = new Intent();
			// i.setClassName("com.example.proximityalert",
			// "com.example.proximityalert.Hey");
			// i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			// context.startActivity(i);
			//
		} else {
			// Other custom Notification
			Log.i("MyTag", "Thank you for visiting my Area,come back again !!");
			// Toast.makeText(context,
			// "Thank you for visiting my Area,come back again !!",
			// Toast.LENGTH_SHORT).show();
		}
	}

	private void sendNotification() {
		// TODO Auto-generated method stub
		RequestParams params = new RequestParams();

		params.put("macaddr", "000000000000011");

		params.put("latitude", "23.2452345235");
		params.put("longitude", "80.23423242323");

		sendMe(params);

	}

	private void sendMe(RequestParams params) {
		// TODO Auto-generated method stub
		AsyncHttpClient client = new AsyncHttpClient();

		client.post(
				"http://a92c7aa3a5d5429cba31efe555e0cb58.cloudapp.net/api/Common/Facade/InformStoreManager",
				params, new AsyncHttpResponseHandler() {

					@Override
					public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
						System.out.println("@@##RESPONSE" + responseBody.toString());
					}

					@Override
					public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

					}

				});
	}

}