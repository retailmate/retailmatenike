package com.cognizant.iot.splashscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.cognizant.iot.activity.Login_Screen;
import com.cognizant.retailmate.R;

/**
 * Created by 452781 on 9/13/2016.
 */
public class SplashScreen extends Activity {
    int tick;
    ImageView splashcognitwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.splashscreen);


        RotateAnimation anim = new RotateAnimation(0f, 360f, 15f, 15f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(3000);

        // Start animating the image
        final ImageView splash = (ImageView) findViewById(R.id.splashImage);
//        splash.startAnimation(anim);


        splashcognitwo = (ImageView) findViewById(R.id.splashImageCognizanttwo);

        tick = 0;
        new CountDownTimer(3000, 200) {

            public void onTick(long millisUntilFinished) {
                tick++;


                if (tick == 12) {

                    splashcognitwo.setVisibility(View.VISIBLE);
                }

            }

            public void onFinish() {
                Intent intent = new Intent(getApplicationContext(), Login_Screen.class);
//                startActivity(intent);
//                splash.setAnimation(null);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(SplashScreen.this, SplashScreen.this.findViewById(android.R.id.content), "profile");
                startActivity(intent, options.toBundle());


                finish();
            }
        }.start();


    }
}
