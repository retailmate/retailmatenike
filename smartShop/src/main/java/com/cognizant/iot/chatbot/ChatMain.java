package com.cognizant.iot.chatbot;


import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;


import com.cognizant.iot.chatbot.adapter.ChatAdapter;
import com.cognizant.iot.chatbot.model.ChatDataModel;
import com.cognizant.iot.chatbot.model.OfferDataModel;
import com.cognizant.iot.chatbot.model.ProductDataModel;
import com.cognizant.retailmate.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.StringTokenizer;

public class ChatMain extends AppCompatActivity {

    Gson gson = new Gson();
    ChatUtil util = new ChatUtil();
    private String jsonResponseString;
    private static final String CustomTag = "CustomTag";
    private String userSays = null;
    private String url = "";
    String reply = null;
    String jsonreplyString = null;
    JSONObject replyObj = null;
    int somethingYouWannaKnow = 0;
    static final int check = 111;
    LocalBroadcastManager mLocalBroadcastManager;
    protected boolean isSuggestClicked = false;
    public ProgressDialog mProgressDialog;
    private FirebaseAuth mAuth;
    // [END declare_auth]

    // [START declare_auth_listener]
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;

    private static final int RC_SIGN_IN = 9001;


    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.e("%%%%", intent.getStringExtra("reply"));
            ChatGlobal.chatText.setText(intent.getStringExtra("reply"));
            isSuggestClicked = true;
            ChatGlobal.sendButton.performClick();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatbot_activity_chat_main);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        //Broadcast Manager for simulating onClick for sendButton
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("suggestion.click");
        mLocalBroadcastManager.registerReceiver(mBroadcastReceiver, mIntentFilter);

        //FOR INITIALIZING mContext in ChatGlobal.java
        ChatGlobal chatGlobal = new ChatGlobal(getApplicationContext());

        ChatGlobal.chatDataModels = new ArrayList<>();
        ChatGlobal.sendButton = (ImageButton) findViewById(R.id.enter_chat1);
        ChatGlobal.mDatasetTypes = new ArrayList<Integer>();
        ChatGlobal.mDataset = new ArrayList<String>();
        ChatGlobal.mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        ChatGlobal.mLayoutManager = new LinearLayoutManager(ChatMain.this);

        ChatGlobal.mRecyclerView.setLayoutManager(ChatGlobal.mLayoutManager);
        //Adapter is created in the last step
        ChatGlobal.mAdapter = new ChatAdapter(ChatMain.this, ChatGlobal.chatDataModels);
        ChatGlobal.mRecyclerView.setAdapter(ChatGlobal.mAdapter);
        ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
        ChatGlobal.chatText = (EditText) findViewById(R.id.chat_edit_text);

        //FOR TEXT TO SPEECH
        ChatGlobal.textToSpeech = new TextToSpeech(ChatGlobal.mContext, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == ChatGlobal.textToSpeech.SUCCESS) {
                    ChatGlobal.result = ChatGlobal.textToSpeech.setLanguage(Locale.UK);
                } else {
                    Toast.makeText(ChatGlobal.mContext, "Not supported in your device", Toast.LENGTH_SHORT).show();
                }
            }
        });

        //For Speech to text
        ImageView imageView = (ImageView) findViewById(R.id.voice);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                i.putExtra(RecognizerIntent.EXTRA_PROMPT, "You should be speaking!!");
                try {
                    startActivityForResult(i, check);
                } catch (ActivityNotFoundException a) {
                    a.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Problem with Voice!!!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        util.setReplyMessage("Hi Amer !\nI'm your personal retail assistant.");
        //FOR SUGGESTION BOXES
        ChatGlobal.suggestDataModels = new ArrayList<>();
        util.setSuggestDataModels();
        util.setSuggestion(ChatGlobal.suggestDataModels);
        ChatGlobal.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ChatGlobal.online) {

                    if (isOnline()) {
                        userSays = ChatGlobal.chatText.getText().toString();
                        if (!userSays.isEmpty()) {
                            ChatGlobal.tapToAdd = false;
                            ChatGlobal.chatText.setText("");
                            if (isSuggestClicked) {
                                isSuggestClicked = false;
                            } else {
                                util.setSendMessage(userSays);
                            }
                            userSays = userSays.toLowerCase();
                            //to reply basic queries such as name ,location etc
                            if ((userSays.contains("your name")) || (userSays.contains("ur name"))) {
                                reply = "RetailMate assistant is my name\nand helping you is my game.";
                                util.setReplyMessage(reply);
                            } else if (userSays.contains("your location") || userSays.contains("you located")) {
                                reply = "I am located at cognizant.";
                                util.setReplyMessage(reply);
                            } else if (userSays.contains("associate")) {
                                Intent i = new Intent(ChatMain.this, UserAssociateChat.class);
                                startActivity(i);

                            } else {
                                //to check that user responded to question or not.
                                if (ChatGlobal.isPrompt) {
                                    url = util.stringToUrlContextId(ChatGlobal.baseURL, userSays, ChatGlobal.contextId);
                                    ChatGlobal.isPrompt = false;
                                } else if (ChatGlobal.askedSpecific) {
                                    ChatGlobal.askedSpecific = false;
                                    url = util.stringToUrlContextId(ChatGlobal.baseURL, userSays, ChatGlobal.contextId);
                                    //isAsked = true;
                                } else {
                                    if (ChatGlobal.isAsked) {

                                        if (userSays.contains("yes") || userSays.contains("yeah") || userSays.contains("yup") || userSays.contains("no") || userSays.contains("nope") || userSays.contains("nopes") || userSays.contains("sure")) {
                                            ChatGlobal.isAsked = true;
                                        } else
                                            ChatGlobal.isAsked = false;
                                    }
                                    //if user is responding to previous question then it will go to else part.
                                    if (!ChatGlobal.isAsked) {
                                        url = util.stringToUrl(ChatGlobal.baseURL, userSays);
                                    } else {
                                        url = util.stringToUrlContextId(ChatGlobal.baseURL, userSays, ChatGlobal.contextId);
                                    }
                                }
                                MyAsyncTask task = new MyAsyncTask();
                                task.execute(url);
                            }
                        }
                    } else {
                        reply = "I am not able to reach my server. Please check your connection.";
                        util.setReplyMessage(reply);
                    }
                } else {
                    userSays = ChatGlobal.chatText.getText().toString();
                    if (!userSays.isEmpty()) {
                        ChatGlobal.tapToAdd = false;

                        ChatGlobal.chatText.setText("");
                        util.setSendMessage(userSays);
                        userSays = userSays.toLowerCase();
                        //to reply basic queries such as name ,location etc
                        if ((userSays.contains("your name")) || (userSays.contains("ur name"))) {
                            reply = "RetailMate assistant is my name\nand helping you is my game.";
                            util.setReplyMessage(reply);
                        } else if (userSays.contains("your location") || userSays.contains("you located")) {
                            reply = "I am located at cognizant.";
                            util.setReplyMessage(reply);
                        } else if (userSays.contains("hi") || userSays.contains("hello") || userSays.contains("hey")) {

                            try {
                                jsonreplyString = util.loadJSONFromAsset("greetings.json");
                                replyObj = new JSONObject(jsonreplyString);
                                JSONArray greetingsArray = replyObj.getJSONArray("greetings");
                                Random rand = new Random();
                                int value = rand.nextInt(greetingsArray.length());
                                //Json obj for displaying various greetings
                                JSONObject displayObj = greetingsArray.getJSONObject(value);
                                reply = displayObj.getString("greet");
                                util.setReplyMessage(reply);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (userSays.contains("offer") || userSays.contains("offers") || userSays.contains("deals") || userSays.contains("deals")) {
                            jsonreplyString = util.loadJSONFromAsset("offers.json");
                            try {
                                replyObj = new JSONObject(jsonreplyString);
                                JSONArray offersArray = replyObj.getJSONArray("offers");

                                //box model for offers
                                ChatDataModel chatOfferData = new ChatDataModel();
                                ArrayList<OfferDataModel> offerDataModelArrayList = new ArrayList<>();
                                chatOfferData.setmDatasetTypes(ChatGlobal.RECEIVE_OFFER);
                                Date d1 = new Date();
                                String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
                                chatOfferData.setmTime(time1);

                                util.setReplyMessage("Offers especially for you!\n");
                                for (int i = 0; i < offersArray.length(); i++) {
                                    JSONObject offerObj = offersArray.getJSONObject(i);
                                    OfferDataModel offerDataModel = new OfferDataModel();
                                    offerDataModel.setOfferDescription(offerObj.getString("offer"));
                                    String uri = offerObj.getString("Image");
                                    offerDataModel.setImageSource(util.getResourceId(uri, "drawable", getPackageName()));
                                    offerDataModelArrayList.add(offerDataModel);
                                }
                                util.setReplyOffer(offerDataModelArrayList, chatOfferData);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            util.clearList();
                        } else if (userSays.contains("recommend") || userSays.contains("recommendations") || userSays.contains("recommended") || userSays.contains("suggest")) {

                            jsonreplyString = util.loadJSONFromAsset("recommend.json");
                            try {
                                replyObj = new JSONObject(jsonreplyString);
                                JSONArray recommendArray = replyObj.getJSONArray("value");

                                //box model for offers
                                ChatDataModel chatRecommendData = new ChatDataModel();
                                ArrayList<ProductDataModel> productDataModelArrayList = new ArrayList<>();
                                chatRecommendData.setmDatasetTypes(ChatGlobal.RECEIVE_PRODUCT);
                                Date d1 = new Date();
                                String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
                                chatRecommendData.setmTime(time1);

                                util.setReplyMessage("Recommended Products are \n");
                                for (int i = 0; i < recommendArray.length(); i++) {
                                    JSONObject productObj = recommendArray.getJSONObject(i);
                                    ProductDataModel productDataModel = new ProductDataModel();
                                    productDataModel.setProductName(productObj.getString("ProductName"));
                                    productDataModel.setProductId(productObj.getString("ProductId"));
                                    productDataModel.setPrice(productObj.getDouble("Custprice"));
                                    String uri = productObj.getString("Image");
                                    Log.e(CustomTag, "image = " + uri);
                                    productDataModel.setImageSource(util.getResourceId(uri, "drawable", getPackageName()));
                                    Log.e(CustomTag, "productDataModel image = " + productDataModel.getImageSource());

                                    productDataModelArrayList.add(productDataModel);
                                }
                                util.setReplyProductOffline(productDataModelArrayList, chatRecommendData);
                                util.setReplyMessage("If you want to add any of these items to your cart, please connect to network.");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            util.clearList();
                        } else if (userSays.contains("cool")) {
                            util.setReplyMessage("Happy to help you.");
                        } else if (userSays.contains("thank") || userSays.contains("thank you") || userSays.contains("thanks")) {
                            util.setReplyMessage("Your welcome. \nI'm Glad I was able to help. ");
                        } else if (userSays.contains("bye")) {
                            util.setReplyMessage("Bye. Hope to see you soon.");
                        } else {
                            util.setReplyMessage("I don't understand. Please go online to explore further.");
                        }
                    }
                }
                //hide keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(ChatGlobal.sendButton.getApplicationWindowToken(), 0);
            }
        });

        ChatGlobal.mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override

            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
            }
        });
    }

    //Result from speech to text activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == check && resultCode == RESULT_OK) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            ChatGlobal.chatText.setText(result.get(0));
            //For Simulating sendButton onClick
            ChatGlobal.sendButton.performClick();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        String outStateString = gson.toJson(ChatGlobal.chatDataModels);
        outState.putString("chat", outStateString);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            String chatData = savedInstanceState.getString("chat");
            Type listType = new TypeToken<ArrayList<ChatDataModel>>() {
            }.getType();
            List<ChatDataModel> chatDataModelPref = gson.fromJson(chatData, listType);
            for (int i = 0; i < chatDataModelPref.size(); i++) {
                ChatGlobal.chatDataModels.add(chatDataModelPref.get(i));
            }
            ChatGlobal.mAdapter.notifyDataSetChanged();
            ChatGlobal.mRecyclerView.scrollToPosition(ChatGlobal.chatDataModels.size() - 1);
        }
    }

    protected boolean isOnline() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null && info.isConnectedOrConnecting()) {
            return true;
        } else
            return false;
    }

    class MyAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            BaseConnection con = new BaseConnection();
            String jsonString = "server not working";
            try {
                jsonString = con.run(strings[0]);
                return jsonString;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return jsonString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //to avoid app crash
            if (s.equalsIgnoreCase("server not working")) {

                ChatGlobal.mDataset.add("Sorry, please try later. I am a little busy at the moment.");
                ChatGlobal.mDatasetTypes.add(ChatGlobal.RECEIVE);
                ChatGlobal.mAdapter.notifyDataSetChanged();
                ChatGlobal.mRecyclerView.smoothScrollToPosition(ChatGlobal.mDataset.size() - 1);
            } else {
                jsonResponseString = s;

                //Getting Response from LUIS
                JSONObject obj = null;
                try {

                    obj = new JSONObject(jsonResponseString);
                    JSONObject intentObj = (JSONObject) obj.get("topScoringIntent");
                    JSONArray entityArray = (JSONArray) obj.get("entities");
                    String intent = intentObj.getString("intent").toLowerCase();
                    Log.d(CustomTag, "intent = " + intent);
                    ProductDataModel dataModel = new ProductDataModel();

                    /**
                     *   Preparing reply string to be shown to user.
                     */

                    // intent refers to the action user wants.
                    switch (intent) {

                        case "greetings":
                            ChatGlobal.isAsked = false;
                            if (userSays.contains("morning")) {
                                reply = "Good Morning \nHow can I help? ";
                                util.setReplyMessage(reply);
                            } else if (userSays.contains("evening")) {
                                reply = "Nice to see you \nWhat can i do for you? ";
                                util.setReplyMessage(reply);
                            } else if (userSays.contains("noon")) {
                                reply = "Hello \nWhat can i do for you ?";
                                util.setReplyMessage(reply);
                            } else if (userSays.contains("night")) {
                                reply = "Good Night \nHope to see you soon. ";
                                util.setReplyMessage(reply);
                            } else if (userSays.contains("bye")) {
                                reply = "Bye. \nHope to see you soon. ";
                                util.setReplyMessage(reply);
                            } else if (userSays.contains("thank") || userSays.contains("thank you")) {
                                reply = "Your welcome. \nI'm Glad I was able to help. ";
                                util.setReplyMessage(reply);
                            } else {
                                jsonreplyString = util.loadJSONFromAsset("greetings.json");
                                replyObj = new JSONObject(jsonreplyString);
                                JSONArray greetingsArray = replyObj.getJSONArray("greetings");
                                Random rand = new Random();
                                int value = rand.nextInt(greetingsArray.length());
                                //Json obj for displaying various greetings
                                JSONObject displayObj = greetingsArray.getJSONObject(value);
                                reply = displayObj.getString("greet");
                                util.setReplyMessage(reply);
                            }
                            ChatGlobal.lastIntent = "greetings";
                            break;

                        case "offers":
                            ChatGlobal.isAsked = false;
                            if (obj.has("dialog")) {
                                JSONObject dialogObj = (JSONObject) obj.get("dialog");
                                String status = dialogObj.getString("status").toLowerCase();
                                ChatGlobal.contextId = dialogObj.getString("contextId");
                                if (status.equalsIgnoreCase("question")) {
                                    if (ChatGlobal.countPrompt < 1) {
                                        util.prompt(dialogObj);
                                        ChatGlobal.countPrompt++;
                                    } else {
                                        ChatGlobal.countPrompt = 0;
                                        ChatGlobal.isPrompt = false;
                                    }
                                } else if (status.equalsIgnoreCase("finished")) {

                                    jsonreplyString = util.loadJSONFromAsset("offers.json");
                                    replyObj = new JSONObject(jsonreplyString);
                                    JSONArray offersArray = replyObj.getJSONArray("offers");
                                    StringBuffer sb = new StringBuffer();
                                    //box model for offers
                                    ChatDataModel chatOfferData = new ChatDataModel();
                                    ArrayList<OfferDataModel> offerDataModelArrayList = new ArrayList<>();
                                    chatOfferData.setmDatasetTypes(ChatGlobal.RECEIVE_OFFER);
                                    Date d1 = new Date();
                                    String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
                                    chatOfferData.setmTime(time1);
                                    //to check for specific offers
                                    if (entityArray.length() > 0) {
                                        //populating entityLists
                                        util.populateEntityList(entityArray);
                                        //add code
                                        if (ChatGlobal.entityItemList.size() > 0) {
                                            for (int i = 0; i < offersArray.length(); i++) {
                                                JSONObject offerObj = offersArray.getJSONObject(i);
                                                String offer = offerObj.getString("offer");
                                                String offerLowerCase = offer.toLowerCase();
                                                for (int j = 0; j < ChatGlobal.entityItemList.size(); j++) {
                                                    int count = 0;
                                                    StringTokenizer tokenizer = new StringTokenizer(ChatGlobal.entityItemList.get(j), " ");
                                                    int wordCount = tokenizer.countTokens();

                                                    while (tokenizer.hasMoreTokens()) {
                                                        if (offerLowerCase.contains(tokenizer.nextToken())) {
                                                            count++;
                                                        }
                                                    }
                                                    if (wordCount == count) {
                                                        // sb.append(offer).append("\n");
                                                        OfferDataModel offerDataModel = new OfferDataModel();
                                                        offerDataModel.setOfferDescription(offer);
                                                        String uri = offerObj.getString("Image");
                                                        offerDataModel.setImageSource(util.getResourceId(uri, "drawable", getPackageName()));
                                                        offerDataModelArrayList.add(offerDataModel);
                                                        Log.d(CustomTag, "matched offer = " + offer);
                                                    }
                                                    if (sb.length() > 0) {
                                                        reply = "I found these offers, especially for you!\n";
                                                        util.setReplyMessage(reply);
                                                        util.setReplyOffer(offerDataModelArrayList, chatOfferData);
                                                    } else {
                                                        reply = "Sorry, I can't find offers at the moment. Please check again sometime next week.";
                                                        util.setReplyMessage(reply);
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        util.setReplyMessage("Offers especially for you!\n");
                                        for (int i = 0; i < offersArray.length(); i++) {
                                            JSONObject offerObj = offersArray.getJSONObject(i);
                                            OfferDataModel offerDataModel = new OfferDataModel();
                                            offerDataModel.setOfferDescription(offerObj.getString("offer"));
                                            String uri = offerObj.getString("Image");
                                            offerDataModel.setImageSource(util.getResourceId(uri, "drawable", getPackageName()));
                                            offerDataModelArrayList.add(offerDataModel);
                                        }
                                        util.setReplyOffer(offerDataModelArrayList, chatOfferData);
                                    }
                                    util.clearList();
                                }
                            }
                            ChatGlobal.lastIntent = "offers";
                            break;

                        case "isavailable":

                            Log.d(CustomTag, "------------>>>>>isAvailable<<<<<<<-------------");
                            if (obj.has("dialog")) {
                                JSONObject dialogObj = (JSONObject) obj.get("dialog");
                                ChatGlobal.contextId = dialogObj.getString("contextId");
                                if (entityArray.length() > 0) {
                                    //populating entityLists
                                    util.populateEntityList(entityArray);
                                }

                                String convertedString = "";
                                //Method for creating SearchAPI
                                Log.e(CustomTag, "isAvailable-------------->>>>> userSays = " + userSays);
                                convertedString = util.makeProductSearchApi(userSays);
                                ProductAsyncTask availableTask = new ProductAsyncTask();

                                if (!ChatGlobal.isAsked) {
                                    ChatGlobal.intentIsAvailable = true;
                                    availableTask.execute(convertedString);
                                } else if (ChatGlobal.isAsked) {
                                    String bool;
                                    int matchCount = 0;
                                    StringBuffer itemBuffer = new StringBuffer();
                                    StringBuffer itemIdBuffer = new StringBuffer();
                                    if (!ChatGlobal.entityBooleanList.isEmpty() && !ChatGlobal.entityItemList.isEmpty()) {
                                        ChatGlobal.isAsked = false;
                                        Log.d(CustomTag, "-------!entityBooleanList.isEmpty() && !entityItemList.isEmpty() = true ---------");

                                        for (int k = 0; k < ChatGlobal.prevProductList.size(); k++) {

                                            String name = ChatGlobal.prevProductList.get(k).getProductName().toLowerCase();
                                            for (int j = 0; j < ChatGlobal.entityItemList.size(); j++) {
                                                int count = 0;
                                                StringTokenizer tokenizer = new StringTokenizer(ChatGlobal.entityItemList.get(j), " ");
                                                int wordCount = tokenizer.countTokens();

                                                while (tokenizer.hasMoreTokens()) {
                                                    if (name.contains(tokenizer.nextToken())) {
                                                        count++;
                                                    }
                                                }
                                                if (wordCount == count) {
                                                    matchCount++;
                                                    ChatGlobal.itemMatchedList.add(ChatGlobal.prevProductList.get(k).getProductName());
                                                    ChatGlobal.itemMatchedIdList.add(ChatGlobal.prevProductList.get(k).getProductId());
                                                    String productId = ChatGlobal.prevProductList.get(k).getProductId();
                                                    dataModel = new ProductDataModel();
                                                    dataModel.setProductName(ChatGlobal.prevProductList.get(k).getProductName());
                                                    dataModel.setPrice(ChatGlobal.prevProductList.get(k).getPrice());
                                                    dataModel.setProductImageResource(ChatGlobal.prevProductList.get(k).getProductImageResource());
                                                    dataModel.setProductId(productId);
                                                    ChatGlobal.productList.add(dataModel);
                                                }
                                            }
                                        }
                                        if (matchCount == ChatGlobal.entityItemList.size()) {

                                            for (int i = 0; i < ChatGlobal.itemMatchedList.size() - 1; i++) {
                                                itemBuffer.append(ChatGlobal.itemMatchedList.get(i)).append(",");
                                                itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(i)).append(",");
                                            }
                                            itemBuffer.append(ChatGlobal.itemMatchedList.get(ChatGlobal.itemMatchedList.size() - 1)).append(" added to cart");
                                            itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(ChatGlobal.itemMatchedIdList.size() - 1)).append(" added to cart");
                                            ChatGlobal.productID = ChatGlobal.itemMatchedIdList.get(0);
                                            ChatGlobal.isaddToCartAPI = true;
                                            String url = ChatGlobal.baseAddToCartAPI + ChatGlobal.googleToken;
                                            CartAsyncTask cartAsyncTask = new CartAsyncTask();
                                            cartAsyncTask.execute(url);
                                            Toast.makeText(ChatMain.this, itemIdBuffer.toString(), Toast.LENGTH_SHORT).show();
                                            reply = itemBuffer.toString();
                                            util.setReplyMessage(reply);
                                            util.setReplyMessage("Please type checkout cart for checking out your current cart or continue.");
                                        } else {
                                            ChatGlobal.tapToAdd = true;
                                            reply = "Click on product that you want to add in your cart.";
                                            util.setReplyMessage(reply);
                                            util.setReplyProduct(ChatGlobal.productList);
                                            Log.d(CustomTag, "productList.size() = " + ChatGlobal.productList.size());
                                        }
                                        util.clearList();
                                        ChatGlobal.prevProductList.clear();
                                    } else if (!ChatGlobal.entityBooleanList.isEmpty()) {
                                        Log.d(CustomTag, "-------!entityBooleanList.isEmpty() = true ---------");

                                        bool = ChatGlobal.entityBooleanList.get(0);
                                        if (bool.equalsIgnoreCase("yes") || bool.equalsIgnoreCase("yeah") || bool.equalsIgnoreCase("yup") || bool.equalsIgnoreCase("sure")) {
                                            reply = "Please mention the item you want to add.";
                                            ChatGlobal.askedSpecific = true;
                                        } else {
                                            reply = "OK. Feel free to ask for any help.";
                                        }
                                        util.setReplyMessage(reply);
                                    }
                                } else if (ChatGlobal.askedSpecific) {
                                    Log.d(CustomTag, "------>>>>> In avaialable <<<<<---------askedSpecific");
                                    int matchCount = 0;
                                    StringBuffer itemBuffer = new StringBuffer();
                                    StringBuffer itemIdBuffer = new StringBuffer();
                                    for (int k = 0; k < ChatGlobal.prevProductList.size(); k++) {

                                        String name = ChatGlobal.prevProductList.get(k).getProductName().toLowerCase();
                                        for (int j = 0; j < ChatGlobal.entityItemList.size(); j++) {
                                            int count = 0;
                                            StringTokenizer tokenizer = new StringTokenizer(ChatGlobal.entityItemList.get(j), " ");
                                            int wordCount = tokenizer.countTokens();

                                            while (tokenizer.hasMoreTokens()) {
                                                if (name.contains(tokenizer.nextToken())) {
                                                    count++;
                                                }
                                            }
                                            if (wordCount == count) {
                                                matchCount++;
                                                ChatGlobal.itemMatchedList.add(ChatGlobal.prevProductList.get(k).getProductName());
                                                ChatGlobal.itemMatchedIdList.add(ChatGlobal.prevProductList.get(k).getProductId());
                                                String productId = ChatGlobal.prevProductList.get(k).getProductId();
                                                dataModel = new ProductDataModel();
                                                dataModel.setProductName(ChatGlobal.prevProductList.get(k).getProductName());
                                                dataModel.setPrice(ChatGlobal.prevProductList.get(k).getPrice());
                                                dataModel.setProductImageResource(ChatGlobal.prevProductList.get(k).getProductImageResource());
                                                dataModel.setProductId(productId);
                                                ChatGlobal.productList.add(dataModel);
                                            }
                                        }
                                    }

                                    if (matchCount == ChatGlobal.entityItemList.size()) {

                                        for (int i = 0; i < ChatGlobal.itemMatchedList.size() - 1; i++) {
                                            itemBuffer.append(ChatGlobal.itemMatchedList.get(i)).append(",");
                                            itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(i)).append(",");
                                        }
                                        itemBuffer.append(ChatGlobal.itemMatchedList.get(ChatGlobal.itemMatchedList.size() - 1)).append(" added to cart");
                                        itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(ChatGlobal.itemMatchedIdList.size() - 1)).append(" added to cart");
                                        ChatGlobal.productID = ChatGlobal.itemMatchedIdList.get(0);
                                        ChatGlobal.isaddToCartAPI = true;
                                        String url = ChatGlobal.baseAddToCartAPI + ChatGlobal.googleToken;
                                        CartAsyncTask cartAsyncTask = new CartAsyncTask();
                                        cartAsyncTask.execute(url);
                                        Toast.makeText(ChatMain.this, itemIdBuffer.toString(), Toast.LENGTH_SHORT).show();
                                        reply = itemBuffer.toString();
                                        util.setReplyMessage(reply);
                                        util.setReplyMessage("Please type checkout cart for checking out your current cart or continue.");
                                        itemBuffer = null;
                                    } else {
                                        ChatGlobal.tapToAdd = true;
                                        reply = "Click on product that you want to add in your cart.";
                                        util.setReplyMessage(reply);
                                        util.setReplyProduct(ChatGlobal.productList);
                                        Log.d(CustomTag, "productList.size() = " + ChatGlobal.productList.size());
                                    }
                                    util.clearList();
                                    //prevProductList.clear();
                                }
                            }
                            ChatGlobal.lastIntent = "isavailable";
                            break;
                        case "price":
                            if (obj.has("dialog")) {
                                JSONObject dialogObj = (JSONObject) obj.get("dialog");
                                String status = dialogObj.getString("status").toLowerCase();
                                ChatGlobal.contextId = dialogObj.getString("contextId");
                                if (status.equalsIgnoreCase("question")) {

                                    if (ChatGlobal.countPrompt < 1) {
                                        util.prompt(dialogObj);
                                        ChatGlobal.countPrompt++;
                                    } else {
                                        ChatGlobal.countPrompt = 0;
                                        ChatGlobal.isPrompt = false;
                                        util.setReplyMessage("Please say it again.");
                                    }
                                } else if (status.equalsIgnoreCase("finished")) {

                                    if (entityArray.length() > 0) {
                                        //populating entityLists
                                        util.populateEntityList(entityArray);
                                    }
                                    String convertedString = "";
                                    //Method for creating SearchAPI
                                    convertedString = util.makeProductSearchApi(userSays);
                                    ProductAsyncTask availableTask = new ProductAsyncTask();

                                    if (!ChatGlobal.isAsked) {
                                        ChatGlobal.intentIsPrice = true;
                                        availableTask.execute(convertedString);
                                    } else if (ChatGlobal.isAsked) {
                                        String bool;
                                        int matchCount = 0;
                                        StringBuffer itemBuffer = new StringBuffer();
                                        StringBuffer itemIdBuffer = new StringBuffer();
                                        if (!ChatGlobal.entityBooleanList.isEmpty() && !ChatGlobal.entityItemList.isEmpty()) {
                                            ChatGlobal.isAsked = false;
                                            Log.d(CustomTag, "-------!entityBooleanList.isEmpty() && !entityItemList.isEmpty() = true ---------");

                                            for (int k = 0; k < ChatGlobal.prevProductList.size(); k++) {

                                                String name = ChatGlobal.prevProductList.get(k).getProductName().toLowerCase();
                                                for (int j = 0; j < ChatGlobal.entityItemList.size(); j++) {
                                                    int count = 0;
                                                    StringTokenizer tokenizer = new StringTokenizer(ChatGlobal.entityItemList.get(j), " ");
                                                    int wordCount = tokenizer.countTokens();

                                                    while (tokenizer.hasMoreTokens()) {
                                                        if (name.contains(tokenizer.nextToken())) {
                                                            count++;
                                                        }
                                                    }
                                                    if (wordCount == count) {
                                                        matchCount++;
                                                        ChatGlobal.itemMatchedList.add(ChatGlobal.prevProductList.get(k).getProductName());
                                                        ChatGlobal.itemMatchedIdList.add(ChatGlobal.prevProductList.get(k).getProductId());
                                                        String productId = ChatGlobal.prevProductList.get(k).getProductId();
                                                        dataModel = new ProductDataModel();
                                                        dataModel.setProductName(ChatGlobal.prevProductList.get(k).getProductName());
                                                        dataModel.setPrice(ChatGlobal.prevProductList.get(k).getPrice());
                                                        dataModel.setProductImageResource(ChatGlobal.prevProductList.get(k).getProductImageResource());
                                                        dataModel.setProductId(productId);
                                                        ChatGlobal.productList.add(dataModel);
                                                    }
                                                }
                                            }
                                            if (matchCount == ChatGlobal.entityItemList.size()) {

                                                for (int i = 0; i < ChatGlobal.itemMatchedList.size() - 1; i++) {
                                                    itemBuffer.append(ChatGlobal.itemMatchedList.get(i)).append(",");
                                                    itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(i)).append(",");
                                                }
                                                itemBuffer.append(ChatGlobal.itemMatchedList.get(ChatGlobal.itemMatchedList.size() - 1)).append(" added to cart");
                                                itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(ChatGlobal.itemMatchedIdList.size() - 1)).append(" added to cart");
                                                ChatGlobal.productID = ChatGlobal.itemMatchedIdList.get(0);
                                                ChatGlobal.isaddToCartAPI = true;
                                                String url = ChatGlobal.baseAddToCartAPI + ChatGlobal.googleToken;
                                                CartAsyncTask cartAsyncTask = new CartAsyncTask();
                                                cartAsyncTask.execute(url);
                                                Toast.makeText(ChatMain.this, itemIdBuffer.toString(), Toast.LENGTH_SHORT).show();
                                                reply = itemBuffer.toString();
                                                util.setReplyMessage(reply);
                                                util.setReplyMessage("Please type checkout cart for checking out your current cart or continue.");
                                            } else {
                                                ChatGlobal.tapToAdd = true;
                                                reply = "Click on product that you want to add in your cart.";
                                                util.setReplyMessage(reply);
                                                util.setReplyProduct(ChatGlobal.productList);
                                                Log.d(CustomTag, "productList.size() = " + ChatGlobal.productList.size());
                                            }
                                            util.clearList();
                                            ChatGlobal.prevProductList.clear();
                                        } else if (!ChatGlobal.entityBooleanList.isEmpty()) {
                                            Log.d(CustomTag, "-------!entityBooleanList.isEmpty() = true ---------");
                                            bool = ChatGlobal.entityBooleanList.get(0);
                                            if (bool.equalsIgnoreCase("yes") || bool.equalsIgnoreCase("yeah") || bool.equalsIgnoreCase("yup") || bool.equalsIgnoreCase("sure")) {
                                                reply = "Please mention the item you want to add.";
                                                ChatGlobal.askedSpecific = true;
                                            } else {
                                                reply = "OK. No problem.";
                                            }
                                            util.setReplyMessage(reply);
                                        }
                                    } else if (ChatGlobal.askedSpecific) {
                                        Log.d(CustomTag, "------>>>>> In Price <<<<<---------askedSpecific");
                                        int matchCount = 0;
                                        StringBuffer itemBuffer = new StringBuffer();
                                        StringBuffer itemIdBuffer = new StringBuffer();
                                        for (int k = 0; k < ChatGlobal.prevProductList.size(); k++) {

                                            String name = ChatGlobal.prevProductList.get(k).getProductName().toLowerCase();
                                            for (int j = 0; j < ChatGlobal.entityItemList.size(); j++) {
                                                int count = 0;
                                                StringTokenizer tokenizer = new StringTokenizer(ChatGlobal.entityItemList.get(j), " ");
                                                int wordCount = tokenizer.countTokens();

                                                while (tokenizer.hasMoreTokens()) {
                                                    if (name.contains(tokenizer.nextToken())) {
                                                        count++;
                                                    }
                                                }
                                                if (wordCount == count) {
                                                    matchCount++;
                                                    ChatGlobal.itemMatchedList.add(ChatGlobal.prevProductList.get(k).getProductName());
                                                    ChatGlobal.itemMatchedIdList.add(ChatGlobal.prevProductList.get(k).getProductId());
                                                    String productId = ChatGlobal.prevProductList.get(k).getProductId();
                                                    dataModel = new ProductDataModel();
                                                    dataModel.setProductName(ChatGlobal.prevProductList.get(k).getProductName());
                                                    dataModel.setPrice(ChatGlobal.prevProductList.get(k).getPrice());
                                                    dataModel.setProductImageResource(ChatGlobal.prevProductList.get(k).getProductImageResource());
                                                    dataModel.setProductId(productId);
                                                    ChatGlobal.productList.add(dataModel);
                                                }
                                            }
                                        }

                                        if (matchCount == ChatGlobal.entityItemList.size()) {

                                            for (int i = 0; i < ChatGlobal.itemMatchedList.size() - 1; i++) {
                                                itemBuffer.append(ChatGlobal.itemMatchedList.get(i)).append(",");
                                                itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(i)).append(",");
                                            }
                                            itemBuffer.append(ChatGlobal.itemMatchedList.get(ChatGlobal.itemMatchedList.size() - 1)).append(" added to cart");
                                            itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(ChatGlobal.itemMatchedIdList.size() - 1)).append(" added to cart");
                                            ChatGlobal.productID = ChatGlobal.itemMatchedIdList.get(0);
                                            ChatGlobal.isaddToCartAPI = true;
                                            String url = ChatGlobal.baseAddToCartAPI + ChatGlobal.googleToken;
                                            CartAsyncTask cartAsyncTask = new CartAsyncTask();
                                            cartAsyncTask.execute(url);
                                            Toast.makeText(ChatMain.this, itemIdBuffer.toString(), Toast.LENGTH_SHORT).show();
                                            reply = itemBuffer.toString();
                                            util.setReplyMessage(reply);
                                            util.setReplyMessage("Please type checkout cart for checking out your current cart or continue.");
                                            itemBuffer = null;

                                        } else {
                                            ChatGlobal.tapToAdd = true;
                                            reply = "Click on product that you want to add in your cart.";
                                            util.setReplyMessage(reply);
                                            util.setReplyProduct(ChatGlobal.productList);
                                            Log.d(CustomTag, "productList.size() = " + ChatGlobal.productList.size());
                                        }
                                        util.clearList();
                                        //prevProductList.clear();
                                    }
                                }
                            }
                            ChatGlobal.lastIntent = "price";
                            break;
                        case "addtocart":

                            if (obj.has("dialog")) {
                                JSONObject dialogObj = (JSONObject) obj.get("dialog");
                                String status = dialogObj.getString("status").toLowerCase();
                                ChatGlobal.contextId = dialogObj.getString("contextId");
                                if (status.equalsIgnoreCase("question")) {
                                    if (ChatGlobal.countPrompt < 1) {
                                        util.prompt(dialogObj);
                                        ChatGlobal.countPrompt++;
                                    } else {
                                        ChatGlobal.countPrompt = 0;
                                        ChatGlobal.isPrompt = false;
                                        util.setReplyMessage("Please say it again.");
                                    }
                                } else if (status.equalsIgnoreCase("finished")) {
                                    if (entityArray.length() > 0) {
                                        //populating entityLists
                                        util.populateEntityList(entityArray);
                                    }
                                    String convertedString = "";
                                    //Method for creating SearchAPI
                                    convertedString = util.makeProductSearchApi(userSays);
                                    ProductAsyncTask availableTask = new ProductAsyncTask();
                                    ChatGlobal.intentIsAddToCart = true;
                                    availableTask.execute(convertedString);
                                }
                            }
                            ChatGlobal.lastIntent = "addtocart";
                            break;
                        case "recommendations":

                            Log.d(CustomTag, "------------>>>>>Recommendations<<<<<<<-------------");
                            if (obj.has("dialog")) {
                                JSONObject dialogObj = (JSONObject) obj.get("dialog");
                                ChatGlobal.contextId = dialogObj.getString("contextId");
                                if (entityArray.length() > 0) {
                                    //populating entityLists
                                    util.populateEntityList(entityArray);
                                }
                                ProductAsyncTask availableTask = new ProductAsyncTask();

                                if (!ChatGlobal.isAsked) {
                                    ChatGlobal.intentIsRecommend = true;
                                    availableTask.execute(ChatGlobal.recommendAPI);
                                    reply = "Give me a minute. I'll get back to you after getting products suitable for you.  ";
                                    util.setReplyMessage(reply);
                                } else if (ChatGlobal.isAsked) {
                                    String bool;
                                    int matchCount = 0;
                                    StringBuffer itemBuffer = new StringBuffer();
                                    StringBuffer itemIdBuffer = new StringBuffer();
                                    if (!ChatGlobal.entityBooleanList.isEmpty() && !ChatGlobal.entityItemList.isEmpty()) {
                                        ChatGlobal.isAsked = false;
                                        Log.d(CustomTag, "-------!entityBooleanList.isEmpty() && !entityItemList.isEmpty() = true ---------");
                                        for (int k = 0; k < ChatGlobal.prevProductList.size(); k++) {
                                            String name = ChatGlobal.prevProductList.get(k).getProductName().toLowerCase();
                                            for (int j = 0; j < ChatGlobal.entityItemList.size(); j++) {
                                                int count = 0;
                                                StringTokenizer tokenizer = new StringTokenizer(ChatGlobal.entityItemList.get(j), " ");
                                                int wordCount = tokenizer.countTokens();

                                                while (tokenizer.hasMoreTokens()) {
                                                    if (name.contains(tokenizer.nextToken())) {
                                                        count++;
                                                    }
                                                }
                                                if (wordCount == count) {
                                                    matchCount++;
                                                    ChatGlobal.itemMatchedList.add(ChatGlobal.prevProductList.get(k).getProductName());
                                                    ChatGlobal.itemMatchedIdList.add(ChatGlobal.prevProductList.get(k).getProductId());
                                                    String productId = ChatGlobal.prevProductList.get(k).getProductId();
                                                    dataModel = new ProductDataModel();
                                                    dataModel.setProductName(ChatGlobal.prevProductList.get(k).getProductName());
                                                    dataModel.setPrice(ChatGlobal.prevProductList.get(k).getPrice());
                                                    dataModel.setProductImageResource(ChatGlobal.prevProductList.get(k).getProductImageResource());
                                                    dataModel.setProductId(productId);
                                                    ChatGlobal.productList.add(dataModel);
                                                }
                                            }
                                        }

                                        if (matchCount == ChatGlobal.entityItemList.size()) {
                                            for (int i = 0; i < ChatGlobal.itemMatchedList.size() - 1; i++) {
                                                itemBuffer.append(ChatGlobal.itemMatchedList.get(i)).append(",");
                                                itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(i)).append(",");
                                            }
                                            itemBuffer.append(ChatGlobal.itemMatchedList.get(ChatGlobal.itemMatchedList.size() - 1)).append(" added to cart");
                                            itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(ChatGlobal.itemMatchedIdList.size() - 1)).append(" added to cart");
                                            ChatGlobal.productID = ChatGlobal.itemMatchedIdList.get(0);
                                            ChatGlobal.isaddToCartAPI = true;
                                            String url = ChatGlobal.baseAddToCartAPI + ChatGlobal.googleToken;
                                            CartAsyncTask cartAsyncTask = new CartAsyncTask();
                                            cartAsyncTask.execute(url);
                                            Toast.makeText(ChatMain.this, itemIdBuffer.toString(), Toast.LENGTH_SHORT).show();
                                            reply = itemBuffer.toString();
                                            util.setReplyMessage(reply);
                                            util.setReplyMessage("Please type checkout cart for checking out your current cart or continue.");
                                        } else {
                                            ChatGlobal.tapToAdd = true;
                                            reply = "Click on product that you want to add in your cart.";
                                            util.setReplyMessage(reply);
                                            util.setReplyProduct(ChatGlobal.productList);
                                            Log.d(CustomTag, "productList.size() = " + ChatGlobal.productList.size());
                                        }
                                        util.clearList();
                                        ChatGlobal.prevProductList.clear();

                                    } else if (!ChatGlobal.entityBooleanList.isEmpty()) {
                                        Log.d(CustomTag, "-------!entityBooleanList.isEmpty() = true ---------");
                                        bool = ChatGlobal.entityBooleanList.get(0);
                                        if (bool.equalsIgnoreCase("yes") || bool.equalsIgnoreCase("yeah") || bool.equalsIgnoreCase("yup") || bool.equalsIgnoreCase("sure")) {
                                            reply = "Please mention the item you want to add.";
                                            ChatGlobal.askedSpecific = true;
                                        } else {
                                            reply = "OK. Feel free to ask for any help.";
                                        }
                                        util.setReplyMessage(reply);
                                    }

                                } else if (ChatGlobal.askedSpecific) {
                                    Log.d(CustomTag, "------>>>>> In Recommendations <<<<<---------askedSpecific");
                                    int matchCount = 0;
                                    StringBuffer itemBuffer = new StringBuffer();
                                    StringBuffer itemIdBuffer = new StringBuffer();
                                    for (int k = 0; k < ChatGlobal.prevProductList.size(); k++) {
                                        String name = ChatGlobal.prevProductList.get(k).getProductName().toLowerCase();
                                        for (int j = 0; j < ChatGlobal.entityItemList.size(); j++) {
                                            int count = 0;
                                            StringTokenizer tokenizer = new StringTokenizer(ChatGlobal.entityItemList.get(j), " ");
                                            int wordCount = tokenizer.countTokens();

                                            while (tokenizer.hasMoreTokens()) {
                                                if (name.contains(tokenizer.nextToken())) {
                                                    count++;
                                                }
                                            }
                                            if (wordCount == count) {
                                                matchCount++;
                                                ChatGlobal.itemMatchedList.add(ChatGlobal.prevProductList.get(k).getProductName());
                                                ChatGlobal.itemMatchedIdList.add(ChatGlobal.prevProductList.get(k).getProductId());
                                                String productId = ChatGlobal.prevProductList.get(k).getProductId();
                                                dataModel = new ProductDataModel();
                                                dataModel.setProductName(ChatGlobal.prevProductList.get(k).getProductName());
                                                dataModel.setPrice(ChatGlobal.prevProductList.get(k).getPrice());
                                                dataModel.setProductImageResource(ChatGlobal.prevProductList.get(k).getProductImageResource());
                                                dataModel.setProductId(productId);
                                                ChatGlobal.productList.add(dataModel);
                                            }
                                        }
                                    }

                                    if (matchCount == ChatGlobal.entityItemList.size()) {
                                        for (int i = 0; i < ChatGlobal.itemMatchedList.size() - 1; i++) {
                                            itemBuffer.append(ChatGlobal.itemMatchedList.get(i)).append(",");
                                            itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(i)).append(",");
                                        }
                                        itemBuffer.append(ChatGlobal.itemMatchedList.get(ChatGlobal.itemMatchedList.size() - 1)).append(" added to cart");
                                        itemIdBuffer.append(ChatGlobal.itemMatchedIdList.get(ChatGlobal.itemMatchedIdList.size() - 1)).append(" added to cart");
                                        ChatGlobal.productID = ChatGlobal.itemMatchedIdList.get(0);
                                        ChatGlobal.isaddToCartAPI = true;
                                        String url = ChatGlobal.baseAddToCartAPI + ChatGlobal.googleToken;
                                        CartAsyncTask cartAsyncTask = new CartAsyncTask();
                                        cartAsyncTask.execute(url);
                                        Toast.makeText(ChatMain.this, itemIdBuffer.toString(), Toast.LENGTH_SHORT).show();
                                        reply = itemBuffer.toString();
                                        util.setReplyMessage(reply);
                                        util.setReplyMessage("Please type checkout cart for checking out your current cart or continue.");
                                    } else {
                                        reply = "Click on product that you want to add in your cart.";
                                        util.setReplyMessage(reply);
                                        util.setReplyProduct(ChatGlobal.productList);
                                        ChatGlobal.tapToAdd = true;
                                        Log.d(CustomTag, "productList.size() = " + ChatGlobal.productList.size());
                                    }
                                    util.clearList();
                                    //prevProductList.clear();
                                }
                            }
                            ChatGlobal.lastIntent = "recommendations";
                            break;

                        case "checkoutcart":
                            Toast.makeText(ChatMain.this, "Checking out cart", Toast.LENGTH_SHORT).show();
                            ChatGlobal.lastIntent = "checkoutcart";
                            break;

                        default:
                            ChatGlobal.isAsked = false;
                            if (userSays.toLowerCase().contains("cool") || userSays.toLowerCase().contains("ok") || userSays.toLowerCase().contains("thank") || userSays.toLowerCase().contains("great")) {
                                reply = "Happy to help you.";
                                util.setReplyMessage(reply);
                            } else if (userSays.toLowerCase().contains("continue")) {
                                reply = "ok. Carry on ! ";
                                util.setReplyMessage(reply);
                            } else {
                                util.setReplyMessage("I don't understand. Here's a result from the web");
                                String URL = util.stringToGoogleSearchUrl(ChatGlobal.baseSearchUrl, userSays, ChatGlobal.endSearchUrl);
                                MySearchTask task = new MySearchTask();
                                task.execute(URL);
                            }
                            ChatGlobal.lastIntent = "default";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d(CustomTag, "finished task");
            }
        }
    }

    //Called from ProductListAdapter when onClick is triggered.
    public static void addTocart() {
        if (ChatGlobal.cartBuffer.length() != 0) {
            Log.d(CustomTag, "cartBuffer in main = " + ChatGlobal.cartBuffer.toString());
            ChatUtil.setReplyMessage(ChatGlobal.cartBuffer.toString());
            ChatGlobal.cartBuffer.setLength(0);
            ChatGlobal.isaddToCartAPI = true;
            String url = ChatGlobal.baseAddToCartAPI + ChatGlobal.googleToken;
            ChatMain main = new ChatMain();
            main.callAsyncTask(url);
        }
    }

    public void callAsyncTask(String url) {
        CartAsyncTask cartAsyncTask = new CartAsyncTask();
        cartAsyncTask.execute(url);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (ChatGlobal.textToSpeech != null) {
            ChatGlobal.textToSpeech.stop();
            ChatGlobal.textToSpeech.shutdown();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.aboutusmenu, menu);
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.mail_aboutus:
                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {"retailmate_customersupport@gmail.com"};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Retail Mate Subject");
                intent.putExtra(Intent.EXTRA_TEXT, "Retail Mate Sample message");
                intent.putExtra(Intent.EXTRA_CC, "retailmate_admin@gmail.com");
                intent.setType("text/html");
                startActivity(Intent.createChooser(intent, "Send mail"));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
