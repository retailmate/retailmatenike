package com.cognizant.iot.chatbot;

import android.os.AsyncTask;
import android.util.Log;


import com.cognizant.iot.chatbot.model.ProductDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by 540472 on 12/27/2016.
 */
public class ProductAsyncTask extends AsyncTask<String, Void, String> {

    private static final String CustomTag = "CustomTag";
    ChatUtil util = new ChatUtil();

    @Override
    protected String doInBackground(String... params) {
        BaseConnection con = new BaseConnection();
        if (ChatGlobal.intentIsRecommend) {
            con.isrecommend = true;
        } else if (ChatGlobal.intentIsAvailable || ChatGlobal.intentIsPrice || ChatGlobal.intentIsAddToCart) {
            con.isavailable = true;
        }
        con.token = "id_token " + ChatGlobal.googleToken;
        String jsonString = "server not working";
        try {
            jsonString = con.run(params[0]);

        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e(CustomTag, "ProductAsyncTask ----------->>>> response received is " + jsonString);
        return jsonString;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s.equalsIgnoreCase("server not working")) {
            util.setReplyMessage("Sorry, please try later. I am a little busy at the moment.");
        } else {
            String apiResponse = s;
            //Getting Response from product list Api
            try {
                JSONObject obj = new JSONObject(apiResponse);
                JSONArray productArray = (JSONArray) obj.get("value");
                ProductDataModel productDataModel;
                if (ChatGlobal.intentIsRecommend) {
                    Log.d(CustomTag, "recommend---->>");
                    ChatGlobal.intentIsRecommend = false;
                    if (productArray.length() == 0) {
                        util.setReplyMessage("No such products found.");
                    } else {
                        double low = 0.0;
                        double high = 0.0;

                        if (productArray.length() > 0) {
                            Log.d(CustomTag, "productArray.length()>0 ");
                            ChatGlobal.prevProductList.clear();
                            //filter according to price range
                            if (ChatGlobal.entityNumberList.size() == 2) {
                                Log.d(CustomTag, "entityNumberList.size()==2");
                                if (ChatGlobal.entityNumberList.get(0) < ChatGlobal.entityNumberList.get(1)) {
                                    low = ChatGlobal.entityNumberList.get(0);
                                    high = ChatGlobal.entityNumberList.get(1);
                                } else {
                                    low = ChatGlobal.entityNumberList.get(1);
                                    high = ChatGlobal.entityNumberList.get(0);
                                }
                                Log.d(CustomTag, "low = " + low + "\nHigh = " + high);
                            }

                            for (int i = 0; i < productArray.length(); i++) {
                                JSONObject itemObj = productArray.getJSONObject(i);
                                String name = itemObj.getString("ProductName");
                                double price = itemObj.getDouble("Custprice");
                                Log.d(CustomTag, "price = " + price);
                                String imageUrl = itemObj.getString("Image");
                                String productId = itemObj.getString("ProductId");
                                productDataModel = new ProductDataModel();
                                productDataModel.setProductName(name);
                                productDataModel.setPrice(price);
                                productDataModel.setProductImageResource(imageUrl);
                                productDataModel.setProductId(productId);

                                if (low != 0.0) {
                                    if (price >= low && price <= high) {
                                        ChatGlobal.productList.add(productDataModel);
                                        ChatGlobal.prevProductList.add(productDataModel);
                                        Log.d(CustomTag, "added to productList----> name = " + name);
                                    }
                                } else {
                                    ChatGlobal.productList.add(productDataModel);
                                    ChatGlobal.prevProductList.add(productDataModel);
                                }
                            }
                            if (ChatGlobal.productList.size() == 0) {
                                util.setReplyMessage("No such products found. ");
                            } else {
                                if (ChatGlobal.productList.size() == 1) {
                                    util.setReplyMessage("I found this product for you. ");
                                    util.setReplyProduct(ChatGlobal.productList);
                                } else {
                                    util.setReplyMessage("I found these products for you. ");
                                    util.setReplyProduct(ChatGlobal.productList);
                                }
                                util.setReplyMessage("Do you want me to add any of these products to your cart ?");
                                //askedAvailableToCart=true;
                                ChatGlobal.isAsked = true;
                            }
                            util.clearList();
                            Log.d(CustomTag, "------->>>>>After clearList()<<<<<--------");
                            Log.d(CustomTag, "prevProductList.size = " + ChatGlobal.prevProductList.size());
                        }
                    }
                } else if (ChatGlobal.intentIsAvailable) {

                    Log.d(CustomTag, "asynkTask------> available");
                    ChatGlobal.intentIsAvailable = false;
                    if (productArray.length() == 0) {
                        util.setReplyMessage("No such products found.");
                    } else {
                        double low = 0.0;
                        double high = 0.0;

                        if (productArray.length() > 0) {
                            Log.d(CustomTag, "productArray.length()>0 ");
                            ChatGlobal.prevProductList.clear();
                            //filter according to price range
                            if (ChatGlobal.entityNumberList.size() == 2) {
                                Log.d(CustomTag, "entityNumberList.size()==2");

                                if (ChatGlobal.entityNumberList.get(0) < ChatGlobal.entityNumberList.get(1)) {
                                    low = ChatGlobal.entityNumberList.get(0);
                                    high = ChatGlobal.entityNumberList.get(1);
                                } else {
                                    low = ChatGlobal.entityNumberList.get(1);
                                    high = ChatGlobal.entityNumberList.get(0);
                                }
                                Log.d(CustomTag, "low = " + low + "\nHigh = " + high);
                            }

                            for (int i = 0; i < productArray.length(); i++) {
                                JSONObject itemObj = productArray.getJSONObject(i);
                                String name = itemObj.getString("Name");
                                double price = itemObj.getDouble("Price");
                                String imageUrl = itemObj.getString("PrimaryImageUrl");
                                String recordID = itemObj.getString("RecordId");
                                productDataModel = new ProductDataModel();
                                productDataModel.setProductName(name);
                                productDataModel.setPrice(price);
                                productDataModel.setProductImageResource(imageUrl);
                                productDataModel.setProductId(recordID);

                                if (low != 0.0) {
                                    if (price >= low && price <= high) {
                                        ChatGlobal.productList.add(productDataModel);
                                        ChatGlobal.prevProductList.add(productDataModel);
                                        Log.d(CustomTag, "added to productList----> name = " + name);
                                    }
                                } else {
                                    ChatGlobal.productList.add(productDataModel);
                                    ChatGlobal.prevProductList.add(productDataModel);
                                }

                            }
                            if (ChatGlobal.productList.size() == 0) {
                                util.setReplyMessage("No such products found. ");
                            } else {
                                if (ChatGlobal.productList.size() == 1) {
                                    util.setReplyMessage("I found this product for you. ");
                                    util.setReplyProduct(ChatGlobal.productList);
                                } else {
                                    util.setReplyMessage("I found these products for you. ");
                                    util.setReplyProduct(ChatGlobal.productList);
                                }


                                // prevProductList=productList;
                                Log.d(CustomTag, "prevProductList.size = " + ChatGlobal.prevProductList.size());
                                util.setReplyMessage("Do you want me to add any of these products to your cart ?");
                                //askedAvailableToCart=true;
                                ChatGlobal.isAsked = true;
                            }
                            util.clearList();
                            Log.d(CustomTag, "------->>>>>After clearList()<<<<<--------");
                            Log.d(CustomTag, "prevProductList.size = " + ChatGlobal.prevProductList.size());

                        }

                    }

                } else if (ChatGlobal.intentIsPrice) {

                    Log.d(CustomTag, "asynkTask------> price");
                    ChatGlobal.intentIsPrice = false;
                    if (productArray.length() == 0) {
                        util.setReplyMessage("No such products found.");
                    } else {
                        double low = 0.0;
                        double high = 0.0;

                        if (productArray.length() > 0) {
                            Log.d(CustomTag, "productArray.length()>0 ");
                            ChatGlobal.prevProductList.clear();
                            //filter according to price range
                            if (ChatGlobal.entityNumberList.size() == 2) {
                                Log.d(CustomTag, "entityNumberList.size()==2");

                                if (ChatGlobal.entityNumberList.get(0) < ChatGlobal.entityNumberList.get(1)) {
                                    low = ChatGlobal.entityNumberList.get(0);
                                    high = ChatGlobal.entityNumberList.get(1);
                                } else {
                                    low = ChatGlobal.entityNumberList.get(1);
                                    high = ChatGlobal.entityNumberList.get(0);
                                }
                                Log.d(CustomTag, "low = " + low + "\nHigh = " + high);
                            }


                            for (int i = 0; i < productArray.length(); i++) {
                                JSONObject itemObj = productArray.getJSONObject(i);
                                String name = itemObj.getString("Name");
                                double price = itemObj.getDouble("Price");
                                String imageUrl = itemObj.getString("PrimaryImageUrl");
                                String recordID = itemObj.getString("RecordId");
                                productDataModel = new ProductDataModel();
                                productDataModel.setProductName(name);
                                productDataModel.setPrice(price);
                                productDataModel.setProductImageResource(imageUrl);
                                productDataModel.setProductId(recordID);

                                if (low != 0.0) {
                                    if (price >= low && price <= high) {
                                        ChatGlobal.productList.add(productDataModel);
                                        ChatGlobal.prevProductList.add(productDataModel);
                                        Log.d(CustomTag, "added to productList----> name = " + name);
                                    }
                                } else {
                                    ChatGlobal.productList.add(productDataModel);
                                    ChatGlobal.prevProductList.add(productDataModel);
                                }

                            }
                            if (ChatGlobal.productList.size() == 0) {
                                util.setReplyMessage("No such products found. ");
                            } else {
                                if (ChatGlobal.productList.size() == 1) {
                                    util.setReplyMessage("Price for this article is :");
                                    util.setReplyProduct(ChatGlobal.productList);
                                } else {
                                    util.setReplyMessage("Prices are as :");
                                    util.setReplyProduct(ChatGlobal.productList);
                                }

                                Log.d(CustomTag, "prevProductList.size = " + ChatGlobal.prevProductList.size());
                                util.setReplyMessage("Do you want me to add any of these products to your cart ?");
                                ChatGlobal.isAsked = true;
                            }
                            util.clearList();
                            Log.d(CustomTag, "------->>>>>After clearList()<<<<<--------");
                            Log.d(CustomTag, "prevProductList.size = " + ChatGlobal.prevProductList.size());
                        }
                    }
                } else if (ChatGlobal.intentIsAddToCart) {

                    Log.d(CustomTag, "asynkTask------> AddToCart");
                    ChatGlobal.intentIsAddToCart = false;
                    if (productArray.length() == 0) {
                        util.setReplyMessage("No such products found.");
                    } else {

                        if (productArray.length() > 0) {
                            ChatGlobal.prevProductList.clear();

                            for (int i = 0; i < productArray.length(); i++) {
                                JSONObject itemObj = productArray.getJSONObject(i);
                                String name = itemObj.getString("Name");
                                double price = itemObj.getDouble("Price");
                                String imageUrl = itemObj.getString("PrimaryImageUrl");
                                String recordID = itemObj.getString("RecordId");

                                productDataModel = new ProductDataModel();
                                productDataModel.setProductName(name);
                                productDataModel.setPrice(price);
                                productDataModel.setProductImageResource(imageUrl);
                                productDataModel.setProductId(recordID);

                                ChatGlobal.productList.add(productDataModel);
                                ChatGlobal.prevProductList.add(productDataModel);

                            }
                            if (ChatGlobal.productList.size() == 0) {
                                util.setReplyMessage("No such products found. ");
                            } else {
                                ChatGlobal.tapToAdd = true;
                                if (ChatGlobal.productList.size() == 1) {
                                    util.setReplyMessage("Tap on product to confirm :");
                                } else {
                                    util.setReplyMessage("Tap on products to confirm :");
                                }
                                util.setReplyProduct(ChatGlobal.productList);
                                Log.d(CustomTag, "prevProductList.size = " + ChatGlobal.prevProductList.size());
                            }
                            util.clearList();
                            Log.d(CustomTag, "------->>>>>After clearList()<<<<<--------");
                            Log.d(CustomTag, "prevProductList.size = " + ChatGlobal.prevProductList.size());
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            util.clearList();
        }
    }
}
