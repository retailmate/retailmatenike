package com.cognizant.iot.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.utils.MySSLSocketFactory;

public class JSONParser {

	static InputStream is = null;
	static JSONObject jobj = null;
	static String jsondata = "";
	String imei;

	public JSONParser() {

	}

	public static JSONObject getJSONfromURLAX7(String url) {
		InputStream is = null;
		String result = "";
		JSONObject jArray = null;
		ArrayList<NameValuePair> postParameters;

		// Download JSON data from URL
		try {
			HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
			HttpGet httppost = new HttpGet(url);

			httppost.setHeader("Authorization", CatalogActivity.access_token_AX);

			List<NameValuePair> params = new ArrayList<NameValuePair>();

			UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,
					HTTP.UTF_8);

			// httppost.setEntity(ent);

			/*
			 * httppost.setHeader("Content-Type", "application/json");
			 * HttpEntity entity = new
			 * StringEntity("{\"macaddr\":\" 000000000000788 \"}");
			 * httppost.setEntity(entity);
			 */

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity httpentity = response.getEntity();
			is = httpentity.getContent();

		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new JSONObject(result);
		} catch (JSONException e) {
			Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}

	public String makeHttpRequestAX(String url, String imeinbr)
			throws UnsupportedEncodingException {
		HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();

		System.out.println("@@## URL in get call" + url);
		HttpGet httpPost = new HttpGet(url);

		imei = imeinbr;

		//httpPost.setHeader("Authorization", CatalogActivity.access_token_AX);

		
		// System.out.println("@@## access_token_AX "
		// + CatalogActivity.access_token_AX);

		try {
			HttpResponse httpresponse = httpclient.execute(httpPost);
			System.out.println("@@@@@@@@@@@@@@@@@@@@"
					+ httpresponse.getStatusLine().toString());

			HttpEntity httpentity = httpresponse.getEntity();
			is = httpentity.getContent();

			System.out.println("@@### " + is.toString());

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			return "wrong";
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");

				}
				is.close();
				jsondata = sb.toString();

				System.out.println("@@##JSON DATA" + jsondata);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsondata;

	}

	public String makeHttpRequest(String url, String imeinbr)
			throws UnsupportedEncodingException {
		DefaultHttpClient httpclient = new DefaultHttpClient();
		trustEveryone();
		System.out.println("@@## URL in post call" + url);
		HttpPost httpPost = new HttpPost(url);

		imei = imeinbr;
		/*
		 * Used for conenction to ERP
		 */
		// httpPost.setHeader("User-Agent", "SuiteScript-Call");
		// httpPost.setHeader(
		// "Authorization",
		// "NLAuth nlauth_account=TSTDRV1174894, nlauth_email=Harisankar.B@cognizant.com,nlauth_signature=Cognizant@123, nlauth_role=3");
		httpPost.setHeader("Content-Type", "application/json");
		HttpEntity entity = new StringEntity("{\"macaddr\":" + "\"" + imei
				+ "\"" + "}");
		httpPost.setEntity(entity);

		try {
			HttpResponse httpresponse = httpclient.execute(httpPost);
			System.out.println("@@@@@@@@@@@@@@@@@@@@"
					+ httpresponse.getStatusLine().toString());

			HttpEntity httpentity = httpresponse.getEntity();
			is = httpentity.getContent();

			System.out.println("@@### " + is.toString());

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			return "wrong";
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");

				}
				is.close();
				jsondata = sb.toString();

				System.out.println("@@##JSON DATA" + jsondata);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsondata;

	}

	public static void trustEveryone() {
		try {
			HttpsURLConnection
					.setDefaultHostnameVerifier(new HostnameVerifier() {

						@Override
						public boolean verify(String hostname,
								SSLSession session) {
							// TODO Auto-generated method stub
							return true;
						}
					});
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[] { new X509TrustManager() {
				public void checkClientTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {
				}

				public void checkServerTrusted(X509Certificate[] chain,
						String authType) throws CertificateException {
				}

				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}
			} }, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(context
					.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}