package com.cognizant.iot.orderhistory.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 452781 on 12/21/2016.
 */
public class OrderDetailModel implements Serializable {

    @SerializedName("$id")
    @Expose
    private String $id;
    @SerializedName("ItemId")
    @Expose
    private String itemId;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("ProdCategory")
    @Expose
    private String prodCategory;
    @SerializedName("UnitPrice")
    @Expose
    private Double unitPrice;
    @SerializedName("Qty")
    @Expose
    private Integer qty;
    @SerializedName("LineAmount")
    @Expose
    private Double lineAmount;
    @SerializedName("Image")
    @Expose
    private String image;
    @SerializedName("ProductId")
    @Expose
    private String productId;

    public String get$id() {
        return $id;
    }

    public void set$id(String $id) {
        this.$id = $id;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProdCategory() {
        return prodCategory;
    }

    public void setProdCategory(String prodCategory) {
        this.prodCategory = prodCategory;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getLineAmount() {
        return lineAmount;
    }

    public void setLineAmount(Double lineAmount) {
        this.lineAmount = lineAmount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
