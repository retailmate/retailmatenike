package com.cognizant.iot.orderhistory;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.cognizant.retailmate.R;

public class qrActivity extends AppCompatActivity {
    private ImageView imageView;//responsible for holding the Qrcode image

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        imageView = (ImageView) this.findViewById(R.id.imageView2);
        Bitmap bitmap=getIntent().getParcelableExtra("pic");
        imageView.setImageBitmap(bitmap);
    }
}
