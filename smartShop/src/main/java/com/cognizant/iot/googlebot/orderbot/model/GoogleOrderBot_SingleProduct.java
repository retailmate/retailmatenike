package com.cognizant.iot.googlebot.orderbot.model;

/**
 * Created by Guest_User on 09/01/17.
 */

public class GoogleOrderBot_SingleProduct {
    private String url;
    private String productname;
    private String amount;


    public String getImageurl() {
        return url;
    }

    public void setImageurl(String url) {
        this.url = url;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
