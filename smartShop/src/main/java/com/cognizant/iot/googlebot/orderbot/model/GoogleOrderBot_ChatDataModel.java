package com.cognizant.iot.googlebot.orderbot.model;

import java.util.ArrayList;

/**
 * Created by 543898 on 10/18/2016.
 */
public class GoogleOrderBot_ChatDataModel {
    String mDataset;
    String mTime;
    Integer mDatasetTypes;
    ArrayList<GoogleOrderBot_RecommendModel> recommendArray;
    ArrayList<GoogleOrderBot_SingleProduct> mProductImageArray;
    ArrayList<GoogleOrderBot_HomePicker> homepickerArray;
    ArrayList<GoogleOrderBot_SingleLocation> singleLocationarray;
    ArrayList<GoogleOrderBot_StorePicker> storpickerArray;

    public ArrayList<GoogleOrderBot_StorePicker> getStorpickerArray() {
        return storpickerArray;
    }

    public void setStorpickerArray(ArrayList<GoogleOrderBot_StorePicker> storpickerArray) {
        this.storpickerArray = storpickerArray;
    }

    public ArrayList<GoogleOrderBot_SingleLocation> getSingleLocationarray() {
        return singleLocationarray;
    }

    public void setSingleLocationarray(ArrayList<GoogleOrderBot_SingleLocation> singleLocationarray) {
        this.singleLocationarray = singleLocationarray;
    }

    public ArrayList<GoogleOrderBot_RecommendModel> getRecommendArray() {
        return recommendArray;
    }

    public void setRecommendArray(ArrayList<GoogleOrderBot_RecommendModel> recommendArray) {
        this.recommendArray = recommendArray;
    }

    public ArrayList<GoogleOrderBot_SingleProduct> getmProductImageArray() {
        return mProductImageArray;
    }

    public void setmProductImageArray(ArrayList<GoogleOrderBot_SingleProduct> mProductImageArray) {
        this.mProductImageArray = mProductImageArray;
    }

    public ArrayList<GoogleOrderBot_HomePicker> getHomepickerArray() {
        return homepickerArray;
    }

    public void setHomepickerArray(ArrayList<GoogleOrderBot_HomePicker> homepickerArray) {
        this.homepickerArray = homepickerArray;
    }

    public String getmDataset() {
        return mDataset;
    }

    public void setmDataset(String mDataset) {
        this.mDataset = mDataset;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public Integer getmDatasetTypes() {
        return mDatasetTypes;
    }

    public void setmDatasetTypes(Integer mDatasetTypes) {
        this.mDatasetTypes = mDatasetTypes;
    }


}
