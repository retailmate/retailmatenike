package com.cognizant.iot.googlebot.historyandloyaltybot;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.cognizant.iot.googlebot.historyandloyaltybot.adapter.GoogleBot_ChatAdapter;
import com.cognizant.iot.googlebot.historyandloyaltybot.model.GoogleBot_ChatDataModel;

import com.cognizant.iot.googlebot.historyandloyaltybot.model.GoogleBot_HomePicker;
import com.cognizant.iot.googlebot.historyandloyaltybot.model.GoogleBot_LoyaltyBanner;
import com.cognizant.iot.googlebot.historyandloyaltybot.model.GoogleBot_RecommendModel;
import com.cognizant.iot.googlebot.historyandloyaltybot.model.GoogleBot_SingleProduct;

import com.cognizant.retailmate.R;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class GoogleBot_ChatMain extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    GoogleBot_ChatGlobal chatGlobal;
    GoogleBot_ChatUtil googleBotChatUtil;
    private static final String CustomTag = "CustomTag";
    private String userSays = null;
    String intent, ainumber, aiprice, aiimagelink, speech;
    public static String aiitem;
    String purchasehistoryapi = "http://rmnikeapiapp.azurewebsites.net/api/PurchaseServices/GetUserPurchaseHistoryAPI";
    String recommendationapi = "http://rmnikeapiapp.azurewebsites.net/api/CognitiveServices/GetUserToItemRecommendationsAPI";
    HashMap<String, JsonElement> aijson;
    RequestQueue queue;
    public static String imageparentlink = "https://Nikedevret.cloudax.dynamics.com/MediaServer/";
    int itemfound = 0;
    //RecyclerView productimageholder;
    //RecyclerView.LayoutManager imagelayoutmanager;
    JSONObject finalobject = null;
    JSONObject recommendobject = null;
    public static ArrayList<GoogleBot_SingleProduct> imageurl = null;
    public static ArrayList<GoogleBot_HomePicker> homepickerlist;
    public static ArrayList<GoogleBot_LoyaltyBanner> loyaltybannerarraylist;
    //public static ArrayList<GoogleBot_RecommendModel> recommendurl=null;
    String rewardpoint = "550";
    //AIResponse apiairesponse;
    double latitude = 12.824, longtitude = 80.221;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    public static LatLngBounds current_place;
    public static int PLACE_PICKER_REQUEST = 1;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int userYear, userMonth, userDay, userHour, userMinute;
    TimePickerDialog timePickerDialog;
    DatePickerDialog datePickerDialog;
    String yes_no;
    Intent mapintent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.googlechatbot_activity_chat_main);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        chatGlobal = new GoogleBot_ChatGlobal(getApplicationContext());
        queue = Volley.newRequestQueue(this);
        googleBotChatUtil = new GoogleBot_ChatUtil();
        GoogleBot_ChatGlobal.chatDataModels = new ArrayList<>();
        GoogleBot_ChatGlobal.sendButton = (ImageButton) findViewById(R.id.enter_chat1);
        GoogleBot_ChatGlobal.mDatasetTypes = new ArrayList<Integer>();
        GoogleBot_ChatGlobal.mDataset = new ArrayList<String>();
        GoogleBot_ChatGlobal.mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        GoogleBot_ChatGlobal.mLayoutManager = new LinearLayoutManager(GoogleBot_ChatMain.this);
        GoogleBot_ChatGlobal.mRecyclerView.setLayoutManager(GoogleBot_ChatGlobal.mLayoutManager);
        //Adapter is created in the last step
        GoogleBot_ChatGlobal.mAdapter = new GoogleBot_ChatAdapter(GoogleBot_ChatMain.this, GoogleBot_ChatGlobal.chatDataModels);
        GoogleBot_ChatGlobal.mRecyclerView.setAdapter(GoogleBot_ChatGlobal.mAdapter);
        GoogleBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleBot_ChatGlobal.chatDataModels.size() - 1);
        GoogleBot_ChatGlobal.chatText = (EditText) findViewById(R.id.chat_edit_text);
        //getting user location
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        //

        timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        userHour = hourOfDay;
                        userMinute = minute;
                        GoogleBot_ChatGlobal.chatText.setText(userHour + ":" + userMinute + " " + userDay + "-" + (userMonth + 1) + "-" + userYear);
                        GoogleBot_ChatGlobal.sendButton.callOnClick();
                    }
                }, mHour, mMinute, false);
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                userYear = year;
                userMonth = month;
                userDay = dayOfMonth;
                timePickerDialog.show();
            }
        }, mYear, mMonth, mDay);

        final AIRequest aiRequest = new AIRequest();
        aiRequest.setQuery("Hello");
        AIAsyncTask aiAsyncTask = new AIAsyncTask();
        aiAsyncTask.execute(aiRequest);

        GoogleBot_ChatGlobal.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userSays = GoogleBot_ChatGlobal.chatText.getText().toString();
                GoogleBot_ChatGlobal.chatText.setText("");
                final AIRequest aiRequest = new AIRequest();
                aiRequest.setQuery(userSays);
                AIAsyncTask aiAsyncTask = new AIAsyncTask();
                aiAsyncTask.execute(aiRequest);
                googleBotChatUtil.setSendMessage(userSays);

                //hide keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(GoogleBot_ChatGlobal.sendButton.getApplicationWindowToken(), 0);
            }
        });

        GoogleBot_ChatGlobal.mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override

            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                GoogleBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleBot_ChatGlobal.chatDataModels.size() - 1);
            }
        });
    }

    class AIAsyncTask extends AsyncTask<AIRequest, Void, AIResponse> {   //AICHATBOT framework configuration

        @Override
        protected AIResponse doInBackground(AIRequest... requests) {
            final AIRequest request = requests[0];

            final AIConfiguration config = new AIConfiguration(GoogleBot_ChatGlobal.ACCESS_TOKEN,
                    AIConfiguration.SupportedLanguages.English,
                    AIConfiguration.RecognitionEngine.System);


            final AIDataService aiDataService = new AIDataService(chatGlobal.mContext, config);

            try {
                final AIResponse response = aiDataService.request(request);
                return response;
            } catch (AIServiceException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(AIResponse aiResponse) {       //Response which we are getting
            if (aiResponse != null) {
                // process aiResponse here
                Log.e("TAG", "Response = " + aiResponse.getResult().getFulfillment().getSpeech().toString());
                Log.e("TAG", "Intent = " + aiResponse.getResult().getMetadata().getIntentName().toString());
                speech = aiResponse.getResult().getFulfillment().getSpeech().toString();
                intent = aiResponse.getResult().getMetadata().getIntentName().toString();   //storing the intent name from ai
                aijson = aiResponse.getResult().getParameters();      //getting the json with parameters from ai
                //apiairesponse=aiResponse;
                Log.e("TAG", "actionIncomplete = " + aiResponse.getResult().isActionIncomplete());
                Log.e("TAG", "main result--->" + aiResponse.getResult());
                Log.e("TAG", "result--->" + aiResponse.getResult().getParameters());
                if (intent.equals("Greetings")) {
                    if (aijson.toString() != null) {
                        JSONObject object = null;
                        try {
                            object = new JSONObject(aijson.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            yes_no = object.getString("yes_no");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (yes_no == null) {
                        GoogleBot_ChatUtil.setReplyMessage(speech);
                        startshowingloyaltybanner();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                GoogleBot_ChatUtil.setReplyMessage("We have prepared some products according to your loyalty points! Would you like to see them?");
                            }
                        }, 2000);
                    } else if (yes_no.equals("yes")) {
                        Log.e("Recommend", "<<<");
                        parserecommendjson(recommendationapi);
                        yes_no = null;
                    }
                } else if ((intent.equals("Home Delivery") && speech.equals("Please share your preferred date and time of delivery!")) || (intent.equals("Pickup") && speech.equals("I need your preferred date and time!! Please select accordingly!!"))) {
                    GoogleBot_ChatUtil.setReplyMessage(speech);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            datePickerDialog.show();
                        }
                    }, 2000);
                } else if (intent.equals("Home Delivery") && speech.equals("i need your address for delivery!")) {
                    GoogleBot_ChatUtil.setReplyMessage(speech);
                    startshowinghomepicker();
                } else {
                    if (aijson.toString() != null) {
                        JSONObject aiitemfind = null;
                        try {
                            aiitemfind = new JSONObject(aijson.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            aiitem = aiitemfind.getString("item");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (intent.equals("getproductinfo") && aiitem != null) {
                        Log.e("APIparse", ">>>");
                        parseaijson(aijson.toString());   //method to parse json got from ai
                    }
                    /*else if(intent.equals("Product_recommendation"))
                    {
                        Log.e("Recommend","<<<");
                        parserecommendjson(recommendationapi);
                    }*/
                    else if (aiitem == null) {
                        googleBotChatUtil.setReplyMessage(aiResponse.getResult().getFulfillment().getSpeech());
                    } else
                        googleBotChatUtil.setReplyMessage(aiResponse.getResult().getFulfillment().getSpeech());
                }
            }
        }
    }


    private void startshowinghomepicker() {                          //adapter calling for home picker button
        homepickerlist = new ArrayList<GoogleBot_HomePicker>();
        final GoogleBot_HomePicker homePicker = new GoogleBot_HomePicker();
        homePicker.setImageid(R.drawable.home_picker);
        homepickerlist.add(homePicker);
        GoogleBot_ChatDataModel homepickerRequest = new GoogleBot_ChatDataModel();
        homepickerRequest.setmDatasetTypes(GoogleBot_ChatGlobal.RECIEVE_HOME_PICKER_BUTTON);
        setHomePicker(homepickerlist, homepickerRequest);
    }

    private void startshowingloyaltybanner() {
        loyaltybannerarraylist = new ArrayList<GoogleBot_LoyaltyBanner>();
        final GoogleBot_LoyaltyBanner loyaltybanner = new GoogleBot_LoyaltyBanner();
        loyaltybanner.setLoyaltyimageid(R.drawable.reward_icon);
        loyaltybanner.setLoyaltypoints(rewardpoint);
        loyaltybannerarraylist.add(loyaltybanner);
        GoogleBot_ChatDataModel loyaltyRequest = new GoogleBot_ChatDataModel();
        loyaltyRequest.setmDatasetTypes(GoogleBot_ChatGlobal.LOYALTY_POINTS_SHOW);
        setLoyaltybanner(loyaltybannerarraylist, loyaltyRequest);
    }


    /*public void startShowingMap()
    {
        try {
            PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
            intentBuilder.setLatLngBounds(current_place);
            Intent intent = intentBuilder.build(GoogleBot_ChatMain.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }*/
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {   //result from placepicker
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                chatGlobal.chatText.setText(place.getAddress().toString());
            }
        }
    }

    private void parserecommendjson(String recommendationapi) {
        Map<String, String> jsonParamsrecommend = new HashMap<String, String>();
        jsonParamsrecommend.put("UserId", "004021");
        JsonObjectRequest recommendrequest = new JsonObjectRequest(Request.Method.POST, recommendationapi,

                new JSONObject(jsonParamsrecommend),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("PurchaseJson", response.toString());
                        //json=response.toString();
                        googleBotChatUtil.setReplyMessage("Here are some of the products according to your loyalty points");
                        parserecommendapi(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Log.e("Volley error",error.getMessage());
                        googleBotChatUtil.setReplyMessage("I am finding some problem in connectivity!! Give me some time!!");
                        //   Handle Error
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> recommendheaders = new HashMap<String, String>();
                recommendheaders.put("Content-Type", "application/json; charset=utf-8");
                recommendheaders.put("User-agent", System.getProperty("http.agent"));
                return recommendheaders;
            }
        };
        queue.add(recommendrequest);
    }


    private void parserecommendapi(JSONObject response) {
        ArrayList<GoogleBot_RecommendModel> recommendproductlist = new ArrayList<>();
        JSONArray parentarray = null;
        double price = 0;
        try {
            parentarray = response.getJSONArray("value");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < parentarray.length(); i++) {
            try {
                recommendobject = parentarray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            double custprice = 0;
            try {
                custprice = Double.parseDouble(recommendobject.getString("Custprice"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            price += custprice;
            if (price > Double.parseDouble(rewardpoint)) {
                break;
            } else {
                //recommendurl = preparerecommendurl();
                GoogleBot_RecommendModel recommendproduct = new GoogleBot_RecommendModel();
                try {
                    recommendproduct.setImageurl(recommendobject.getString("Image"));
                    recommendproduct.setProductname(recommendobject.getString("ProductName"));
                    recommendproduct.setAmount(recommendobject.getString("Custprice"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                recommendproductlist.add(recommendproduct);
            }
        }
        GoogleBot_ChatDataModel recommendrequest = new GoogleBot_ChatDataModel();
        recommendrequest.setmDatasetTypes(GoogleBot_ChatGlobal.RECEIVE_OFFER);
        setrecommendation(recommendproductlist, recommendrequest);    //to set the recommendations
    }


    private void parseaijson(String ai) {     //AI json parsing
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(ai);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            aiprice = jsonObject.getString("cost");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            aiitem = jsonObject.getString("item");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            ainumber = jsonObject.getString("number");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            aiimagelink = jsonObject.getString("image");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("TAG", "details---->" + ainumber + aiprice + aiitem + aiimagelink);
        if (aiprice != null || ainumber != null || aiimagelink != null) {
            jsonrequest(purchasehistoryapi);
        }
    }


    private void jsonrequest(String purchasehistoryapi) {                           //api calling for purchase history
        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("UserId", "004021");
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, purchasehistoryapi,

                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("J", response.toString());
                        //json=response.toString();
                        parsepurchase(response);                              //method to parse json got from api calling for purchase history
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Log.e("Volley error",error.getMessage());
                        googleBotChatUtil.setReplyMessage("I am finding some problem in connectivity!! Give me some time!!");
                        //   Handle Error
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };
        queue.add(postRequest);
    }

    private void parsepurchase(JSONObject response) {                //parsing the json related to purchase history
        Log.e("///", response.toString());
        JSONArray parentArray = null;
        try {
            parentArray = response.getJSONArray("value");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < parentArray.length(); i++) {
            //JSONObject finalobject= null;
            try {
                finalobject = parentArray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                if (finalobject.getString("ProductName").contains(aiitem)) {
                    if (ainumber != null) {
                        googleBotChatUtil.setReplyMessage("The quantity of " + aiitem + " you bought last time is " + finalobject.getString("Qty"));
                        ainumber = null;
                    }
                    if (aiprice != null) {
                        googleBotChatUtil.setReplyMessage("The amount you spent on " + aiitem + " is " + finalobject.getString("UnitPrice"));
                        aiprice = null;
                    }
                    if (aiimagelink != null) {
                        googleBotChatUtil.setReplyMessage("Let me show you your product");
                        imageurl = prepareimageurl();
                        GoogleBot_ChatDataModel chatimagerequest = new GoogleBot_ChatDataModel();
                        chatimagerequest.setmDatasetTypes(GoogleBot_ChatGlobal.RECEIVE_PRODUCT_IMAGE);
                        //GoogleBot_ChatGlobal.chatDataModels.add(chatimagerequest);
                        setproductimage(imageurl, chatimagerequest);
                        aiimagelink = null;

                    }
                    aiitem = null;   //not to retain the item name from previous query
                    itemfound = 1;
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (itemfound == 0) {
            googleBotChatUtil.setReplyMessage("We cannot find the item you just mentioned in your purchase history");
        }
        itemfound = 0;
    }

    /*private ArrayList<GoogleBot_RecommendModel> preparerecommendurl() {
        ArrayList<GoogleBot_RecommendModel> recommendurllist=new ArrayList<>();
        GoogleBot_RecommendModel recommendproduct=new GoogleBot_RecommendModel();
        try {
            recommendproduct.setImageurl(recommendobject.getString("Image"));
            recommendproduct.setProductname(recommendobject.getString("ProductName"));
            recommendproduct.setAmount(recommendobject.getString("Custprice"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        recommendurllist.add(recommendproduct);
        return recommendurllist;
    }*/

    private ArrayList<GoogleBot_SingleProduct> prepareimageurl() {
        ArrayList<GoogleBot_SingleProduct> imageurllist = new ArrayList<>();
        GoogleBot_SingleProduct singleproduct = new GoogleBot_SingleProduct();
        try {
            singleproduct.setImageurl(finalobject.getString("Image"));
            singleproduct.setProductname(finalobject.getString("ProductName"));
            singleproduct.setAmount(finalobject.getString("UnitPrice"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        imageurllist.add(singleproduct);
        return imageurllist;
    }

    public void setproductimage(ArrayList<GoogleBot_SingleProduct> urllist, GoogleBot_ChatDataModel detailrequest) {
        detailrequest.setmProductImageArray(urllist);
        GoogleBot_ChatGlobal.chatDataModels.add(detailrequest);
        GoogleBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleBot_ChatGlobal.chatDataModels.size() - 1);
    }

    private void setrecommendation(ArrayList<GoogleBot_RecommendModel> recommendproduct, GoogleBot_ChatDataModel recommendrequest) {
        recommendrequest.setRecommendArray(recommendproduct);
        GoogleBot_ChatGlobal.chatDataModels.add(recommendrequest);
        GoogleBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleBot_ChatGlobal.chatDataModels.size() - 1);
    }

    private void setHomePicker(ArrayList<GoogleBot_HomePicker> homepickerlist, GoogleBot_ChatDataModel homepickerRequest) {
        homepickerRequest.setHomepickerArray(homepickerlist);
        GoogleBot_ChatGlobal.chatDataModels.add(homepickerRequest);
        GoogleBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleBot_ChatGlobal.chatDataModels.size() - 1);
    }

    private void setLoyaltybanner(ArrayList<GoogleBot_LoyaltyBanner> loyaltybannerarraylist, GoogleBot_ChatDataModel loyaltyRequest) {
        loyaltyRequest.setLoyaltyBannerArray(loyaltybannerarraylist);
        GoogleBot_ChatGlobal.chatDataModels.add(loyaltyRequest);
        GoogleBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleBot_ChatGlobal.chatDataModels.size() - 1);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("TAG", "IN");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longtitude = mLastLocation.getLongitude();
            Log.e("PLACE", "LOCATION" + latitude + " " + longtitude);
            current_place = new LatLngBounds(
                    new LatLng(latitude - 0.008, longtitude - 0.014), new LatLng(latitude + 0.015, longtitude + 0.011));
        } else {
            Log.e("LOCATION", "DEFAULT");
            current_place = new LatLngBounds(
                    new LatLng(latitude - 0.008, longtitude - 0.014), new LatLng(latitude + 0.015, longtitude + 0.011));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), "Connection failed!!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.aboutusmenu, menu);
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.mail_aboutus:
                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {"retailmate_customersupport@gmail.com"};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Retail Mate Subject");
                intent.putExtra(Intent.EXTRA_TEXT, "Retail Mate Sample message");
                intent.putExtra(Intent.EXTRA_CC, "retailmate_admin@gmail.com");
                intent.setType("text/html");
                startActivity(Intent.createChooser(intent, "Send mail"));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
