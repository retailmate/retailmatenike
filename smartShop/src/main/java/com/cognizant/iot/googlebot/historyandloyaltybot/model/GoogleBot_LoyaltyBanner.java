package com.cognizant.iot.googlebot.historyandloyaltybot.model;

/**
 * Created by Guest_User on 25/01/17.
 */

public class GoogleBot_LoyaltyBanner {
    int loyaltyimageid;
    String loyaltypoints;

    public int getLoyaltyimageid() {
        return loyaltyimageid;
    }

    public void setLoyaltyimageid(int loyaltyimageid) {
        this.loyaltyimageid = loyaltyimageid;
    }

    public String getLoyaltypoints() {
        return loyaltypoints;
    }

    public void setLoyaltypoints(String loyaltypoints) {
        this.loyaltypoints = loyaltypoints;
    }
}
