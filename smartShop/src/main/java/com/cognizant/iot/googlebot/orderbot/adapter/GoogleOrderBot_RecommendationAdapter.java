package com.cognizant.iot.googlebot.orderbot.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.cognizant.iot.googlebot.orderbot.GoogleOrderBot_ChatMain;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_RecommendModel;
import com.cognizant.retailmate.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Guest_User on 11/01/17.
 */

public class GoogleOrderBot_RecommendationAdapter extends RecyclerView.Adapter<GoogleOrderBot_RecommendationAdapter.RecommendHolder> {

    private Context context;
    private ArrayList<GoogleOrderBot_RecommendModel> recommendationlist;

    public GoogleOrderBot_RecommendationAdapter(Context context, ArrayList<GoogleOrderBot_RecommendModel> recommendationlist) {
        this.context = context;
        this.recommendationlist = recommendationlist;
    }


    @Override
    public RecommendHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.googlechatbot_single_recommend_image, parent, false);
        RecommendHolder vh = new RecommendHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecommendHolder holder, int position) {
        GoogleOrderBot_RecommendModel recommendproduct = recommendationlist.get(position);
        Picasso.with(context).load(GoogleOrderBot_ChatMain.imageparentlink.concat(recommendproduct.getImageurl())).into(holder.singleimage_recommend);
        holder.productname_recommend.setText(recommendproduct.getProductname());
        holder.amount_recommend.setText("Price - " + recommendproduct.getAmount());
    }

    @Override
    public int getItemCount() {
        return recommendationlist.size();
    }

    public class RecommendHolder extends RecyclerView.ViewHolder {
        CardView cardView_recommend;
        ImageView singleimage_recommend;
        TextView productname_recommend;
        TextView amount_recommend;

        public RecommendHolder(final View itemView) {
            super(itemView);
            //cardView_recommend= (CardView) itemView.findViewById(R.id.cardview_recommend);
            singleimage_recommend = (ImageView) itemView.findViewById(R.id.singleimage_recommend);
            productname_recommend = (TextView) itemView.findViewById(R.id.productname_recommend);
            amount_recommend = (TextView) itemView.findViewById(R.id.amount_recommend);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "The item clicked is " + productname_recommend.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
