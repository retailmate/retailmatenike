package com.cognizant.iot.googlebot.orderbot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;


import com.cognizant.iot.googlebot.orderbot.GoogleOrderBot_ChatGlobal;
import com.cognizant.iot.googlebot.orderbot.GoogleOrderBot_ChatMain;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_StorePicker;
import com.cognizant.retailmate.R;

import java.util.ArrayList;

/**
 * Created by Guest_User on 24/01/17.
 */

public class GoogleOrderBot_StorePickerAdapter extends RecyclerView.Adapter<GoogleOrderBot_StorePickerAdapter.GoogleOrderBot_StorePickerViewHolder> {

    private ArrayList<GoogleOrderBot_StorePicker> storepickerlist;
    private Context context;

    public GoogleOrderBot_StorePickerAdapter(Context context, ArrayList<GoogleOrderBot_StorePicker> storepickerlist) {
        this.context = context;
        this.storepickerlist = storepickerlist;
    }

    @Override
    public GoogleOrderBot_StorePickerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.googlechatbot_spinner_layout, parent, false);
        GoogleOrderBot_StorePickerViewHolder vh = new GoogleOrderBot_StorePickerViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(GoogleOrderBot_StorePickerViewHolder holder, int position) {
        final GoogleOrderBot_StorePicker storePicker = storepickerlist.get(position);
        holder.storeimage.setImageResource(storePicker.getStoreimageid());
    }

    @Override
    public int getItemCount() {
        return storepickerlist.size();
    }

    public class GoogleOrderBot_StorePickerViewHolder extends RecyclerView.ViewHolder {

        ImageView storeimage;
        Spinner spinner;
        ArrayAdapter<String> arrayAdapter;
        int check = 0;

        public GoogleOrderBot_StorePickerViewHolder(View itemView) {
            super(itemView);
            storeimage = (ImageView) itemView.findViewById(R.id.store);
            spinner = (Spinner) itemView.findViewById(R.id.location_spinner);
            if (GoogleOrderBot_ChatMain.intent.equals("StoreUpdate")) {
                arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, GoogleOrderBot_ChatMain.locationlist);
            } else {
                arrayAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, GoogleOrderBot_ChatMain.lockerlist);
            }
            spinner.setAdapter(arrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    check += 1;
                    if (check > 1) {
                        String item = parent.getItemAtPosition(position).toString();
                        GoogleOrderBot_ChatGlobal.chatText.setText(item);
                        GoogleOrderBot_ChatGlobal.sendButton.callOnClick();
                        GoogleOrderBot_ChatGlobal.chatText.setText("");
                    }
                    // Showing selected spinner item
                    //Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }
}
