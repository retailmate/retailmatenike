package com.cognizant.iot.googlebot.orderbot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.cognizant.iot.googlebot.orderbot.GoogleOrderBot_ChatGlobal;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_SingleLocation;
import com.cognizant.retailmate.R;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guest_User on 24/01/17.
 */

public class GoogleOrderBot_LocationAdapter extends RecyclerView.Adapter<GoogleOrderBot_LocationAdapter.LocationViewHolder> {
    private Context context;
    private ArrayList<GoogleOrderBot_SingleLocation> locationList;

    public GoogleOrderBot_LocationAdapter(Context context, ArrayList<GoogleOrderBot_SingleLocation> locationList) {
        this.context = context;
        this.locationList = locationList;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.googlechatbot_single_location_template, parent, false);
        LocationViewHolder locationViewHolder = new LocationViewHolder(view);
        return locationViewHolder;
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {
        GoogleOrderBot_SingleLocation singleLocation = locationList.get(position);
        Log.e("Singlelocationdata", singleLocation.getLocation_Name());
        holder.Location_name.setText(singleLocation.getLocation_Name());
        holder.Location_address.setText(singleLocation.getLocation_Address());
    }

    @Override
    public int getItemCount() {
        return locationList.size();
    }

    public class LocationViewHolder extends RecyclerView.ViewHolder {
        TextView Location_name;
        TextView Location_address;

        public LocationViewHolder(View itemView) {
            super(itemView);
            Location_name = (TextView) itemView.findViewById(R.id.location_name);
            Location_address = (TextView) itemView.findViewById(R.id.location_address);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GoogleOrderBot_ChatGlobal.chatText.setText(Location_name.getText().toString());
                }
            });
        }
    }
}
