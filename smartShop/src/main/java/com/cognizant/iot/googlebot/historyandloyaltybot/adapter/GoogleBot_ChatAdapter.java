package com.cognizant.iot.googlebot.historyandloyaltybot.adapter;


import android.content.Context;

import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.cognizant.iot.googlebot.historyandloyaltybot.GoogleBot_ChatGlobal;
import com.cognizant.iot.googlebot.historyandloyaltybot.model.GoogleBot_ChatDataModel;
import com.cognizant.retailmate.R;
import com.squareup.picasso.Picasso;


import java.util.List;


/**
 * Created by 543898 on 9/30/2016.
 **/
public class GoogleBot_ChatAdapter extends RecyclerView.Adapter<GoogleBot_ChatAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";


    protected List<GoogleBot_ChatDataModel> chatDataModelList;

    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    public class SendViewHolder extends ViewHolder {
        TextView sendMessage;
        TextView time;

        public SendViewHolder(View v) {
            super(v);
            this.sendMessage = (TextView) v.findViewById(R.id.textview_message_send);
            this.time = (TextView) v.findViewById(R.id.textview_time);
        }
    }

    public class ReceiveViewHolder extends ViewHolder {
        TextView receiveMessage;
        TextView time;

        public ReceiveViewHolder(View v) {
            super(v);
            this.receiveMessage = (TextView) v.findViewById(R.id.textview_message_receive);
            this.time = (TextView) v.findViewById(R.id.textview_time);
        }
    }

    public class ReceiveProductImageViewHolder extends ViewHolder {

        RecyclerView productRecyclerView;

        public ReceiveProductImageViewHolder(View v) {
            super(v);
            this.productRecyclerView = (RecyclerView) v.findViewById(R.id.productimageholder);
        }

    }


    public class RecieveRecommendationViewHolder extends ViewHolder {
        RecyclerView recommendRecyclerView;

        public RecieveRecommendationViewHolder(View v) {
            super(v);
            this.recommendRecyclerView = (RecyclerView) v.findViewById(R.id.suggestionholder);
        }
    }

    public class HomePickerViewHolder extends ViewHolder {
        RecyclerView homepickerRecyclerView;

        public HomePickerViewHolder(View v) {
            super(v);
            this.homepickerRecyclerView = (RecyclerView) v.findViewById(R.id.productimageholder);
        }
    }

    public class LoyaltyViewHolder extends ViewHolder {
        RecyclerView loyaltyRecyclerView;

        public LoyaltyViewHolder(View v) {
            super(v);
            this.loyaltyRecyclerView = (RecyclerView) v.findViewById(R.id.productimageholder);
        }
    }

    public GoogleBot_ChatAdapter(Context context, List<GoogleBot_ChatDataModel> chatDataList) {

        this.chatDataModelList = chatDataList;
        this.mContext = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


        View v;
        if (viewType == GoogleBot_ChatGlobal.SEND) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_chat_user_send, viewGroup, false);

            return new SendViewHolder(v);
        } else if (viewType == GoogleBot_ChatGlobal.RECEIVE_PRODUCT_IMAGE) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_product_image_holder, viewGroup, false);
            return new ReceiveProductImageViewHolder(v);
        } else if (viewType == GoogleBot_ChatGlobal.RECEIVE_OFFER) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_suggestion_holder, viewGroup, false);
            return new RecieveRecommendationViewHolder(v);
        } else if (viewType == GoogleBot_ChatGlobal.RECIEVE_HOME_PICKER_BUTTON) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_product_image_holder, viewGroup, false);
            return new HomePickerViewHolder(v);
        } else if (viewType == GoogleBot_ChatGlobal.LOYALTY_POINTS_SHOW) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_product_image_holder, viewGroup, false);
            return new LoyaltyViewHolder(v);
        } else {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_chat_user_receive, viewGroup, false);
            return new ReceiveViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        if (viewHolder.getItemViewType() == GoogleBot_ChatGlobal.SEND) {
            SendViewHolder holder = (SendViewHolder) viewHolder;
            holder.sendMessage.setText(chatDataModelList.get(position).getmDataset());
            holder.time.setText(chatDataModelList.get(position).getmTime());
        } else if (viewHolder.getItemViewType() == GoogleBot_ChatGlobal.RECEIVE_PRODUCT_IMAGE) {
            ReceiveProductImageViewHolder holder = (ReceiveProductImageViewHolder) viewHolder;
            GoogleBot_ProductImageAdapter productImageAdapter = new GoogleBot_ProductImageAdapter(mContext, chatDataModelList.get(position).getmProductImageArray());
            holder.productRecyclerView.setHasFixedSize(true);
            holder.productRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.productRecyclerView.setAdapter(productImageAdapter);
        } else if (viewHolder.getItemViewType() == GoogleBot_ChatGlobal.RECEIVE_OFFER) {
            RecieveRecommendationViewHolder holder = (RecieveRecommendationViewHolder) viewHolder;
            GoogleBot_RecommendationAdapter recommendationAdapter = new GoogleBot_RecommendationAdapter(mContext, chatDataModelList.get(position).getRecommendArray());
            holder.recommendRecyclerView.setHasFixedSize(true);
            holder.recommendRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.recommendRecyclerView.setAdapter(recommendationAdapter);
        } else if (viewHolder.getItemViewType() == GoogleBot_ChatGlobal.RECIEVE_HOME_PICKER_BUTTON) {
            HomePickerViewHolder holder = (HomePickerViewHolder) viewHolder;
            GoogleBot_HomePickerAdapter homePickerAdapter = new GoogleBot_HomePickerAdapter(mContext, chatDataModelList.get(position).getHomepickerArray());
            holder.homepickerRecyclerView.setHasFixedSize(true);
            holder.homepickerRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            holder.homepickerRecyclerView.setAdapter(homePickerAdapter);
        } else if (viewHolder.getItemViewType() == GoogleBot_ChatGlobal.LOYALTY_POINTS_SHOW) {
            LoyaltyViewHolder holder = (LoyaltyViewHolder) viewHolder;
            GoogleBot_LoyaltyBannerAdapter loyaltyBannerAdapter = new GoogleBot_LoyaltyBannerAdapter(mContext, chatDataModelList.get(position).getLoyaltyBannerArray());
            holder.loyaltyRecyclerView.setHasFixedSize(true);
            holder.loyaltyRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            holder.loyaltyRecyclerView.setAdapter(loyaltyBannerAdapter);
        } else {
            ReceiveViewHolder holder = (ReceiveViewHolder) viewHolder;
            holder.receiveMessage.setText(chatDataModelList.get(position).getmDataset());
            holder.time.setText(chatDataModelList.get(position).getmTime());
        }
    }

    @Override
    public int getItemCount() {

        return chatDataModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
//
        return chatDataModelList.get(position).getmDatasetTypes();
    }


}
