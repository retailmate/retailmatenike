package com.cognizant.iot.googlebot.orderbot.model;

/**
 * Created by Guest_User on 24/01/17.
 */

public class GoogleOrderBot_SingleLocation {
    String Location_Name;
    String Location_Address;

    public String getLocation_Address() {
        return Location_Address;
    }

    public void setLocation_Address(String location_Address) {
        Location_Address = location_Address;
    }

    public String getLocation_Name() {
        return Location_Name;
    }

    public void setLocation_Name(String location_Name) {
        Location_Name = location_Name;
    }
}
