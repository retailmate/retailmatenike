package com.cognizant.iot.googlebot.orderbot.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.googlebot.orderbot.GoogleOrderBot_ChatMain;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_SingleProduct;
import com.cognizant.retailmate.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guest_User on 09/01/17.
 */

public class GoogleOrderBot_ProductImageAdapter extends RecyclerView.Adapter<GoogleOrderBot_ProductImageAdapter.ViewHolder> {

    private Context ctx;
    private List<GoogleOrderBot_SingleProduct> imageurllist;

    public GoogleOrderBot_ProductImageAdapter(Context context, ArrayList<GoogleOrderBot_SingleProduct> list) {
        ctx = context;
        imageurllist = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.googlechatbot_single_product_image, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;        // Return the ViewHolder
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final GoogleOrderBot_SingleProduct singleProduct = imageurllist.get(position);
        Picasso.with(ctx).load(GoogleOrderBot_ChatMain.imageparentlink.concat(singleProduct.getImageurl())).into(holder.singleimage);
        holder.productname.setText(singleProduct.getProductname());
        holder.amount.setText("Price - " + singleProduct.getAmount());
    }

    @Override
    public int getItemCount() {
        return imageurllist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView singleimage;
        TextView productname;
        TextView amount;

        public ViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardview);
            singleimage = (ImageView) itemView.findViewById(R.id.singleimage);
            productname = (TextView) itemView.findViewById(R.id.productname);
            amount = (TextView) itemView.findViewById(R.id.amount);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext(), "The item clicked is " + productname.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
