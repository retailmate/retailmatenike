package com.cognizant.iot.googlebot.orderbot.adapter;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.cognizant.iot.googlebot.orderbot.GoogleOrderBot_ChatGlobal;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_ChatDataModel;

import com.cognizant.retailmate.R;

import java.util.List;


/**
 * Created by 543898 on 9/30/2016.
 **/
public class GoogleOrderBot_ChatAdapter extends RecyclerView.Adapter<GoogleOrderBot_ChatAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";


    protected List<GoogleOrderBot_ChatDataModel> chatDataModelList;

    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    public class SendViewHolder extends ViewHolder {
        TextView sendMessage;
        TextView time;

        public SendViewHolder(View v) {
            super(v);
            this.sendMessage = (TextView) v.findViewById(R.id.textview_message_send);
            this.time = (TextView) v.findViewById(R.id.textview_time);
        }
    }

    public class ReceiveViewHolder extends ViewHolder {
        TextView receiveMessage;
        TextView time;

        public ReceiveViewHolder(View v) {
            super(v);
            this.receiveMessage = (TextView) v.findViewById(R.id.textview_message_receive);
            this.time = (TextView) v.findViewById(R.id.textview_time);
        }
    }

    public class ReceiveProductImageViewHolder extends ViewHolder {

        RecyclerView productRecyclerView;

        public ReceiveProductImageViewHolder(View v) {
            super(v);
            this.productRecyclerView = (RecyclerView) v.findViewById(R.id.productimageholder);
        }

    }


    public class RecieveRecommendationViewHolder extends ViewHolder {
        RecyclerView recommendRecyclerView;

        public RecieveRecommendationViewHolder(View v) {
            super(v);
            this.recommendRecyclerView = (RecyclerView) v.findViewById(R.id.suggestionholder);
        }
    }

    public class HomePickerViewHolder extends ViewHolder {
        RecyclerView homepickerRecyclerView;

        public HomePickerViewHolder(View v) {
            super(v);
            this.homepickerRecyclerView = (RecyclerView) v.findViewById(R.id.productimageholder);
        }
    }

    public class StorePickerViewHolder extends ViewHolder {
        RecyclerView storepickerRecyclerView;

        public StorePickerViewHolder(View v) {
            super(v);
            this.storepickerRecyclerView = (RecyclerView) v.findViewById(R.id.productimageholder);
        }
    }

    public class LocationShowViewHolder extends ViewHolder {
        RecyclerView locationRecyclerView;

        public LocationShowViewHolder(View v) {
            super(v);
            this.locationRecyclerView = (RecyclerView) v.findViewById(R.id.locationholder);
        }
    }

    public GoogleOrderBot_ChatAdapter(Context context, List<GoogleOrderBot_ChatDataModel> chatDataList) {

        this.chatDataModelList = chatDataList;
        this.mContext = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


        View v;
        if (viewType == GoogleOrderBot_ChatGlobal.SEND) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_chat_user_send, viewGroup, false);

            return new SendViewHolder(v);
        } else if (viewType == GoogleOrderBot_ChatGlobal.RECEIVE_PRODUCT_IMAGE) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_product_image_holder, viewGroup, false);
            return new ReceiveProductImageViewHolder(v);
        } else if (viewType == GoogleOrderBot_ChatGlobal.RECEIVE_OFFER) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_suggestion_holder, viewGroup, false);
            return new RecieveRecommendationViewHolder(v);
        } else if (viewType == GoogleOrderBot_ChatGlobal.RECIEVE_HOME_PICKER_BUTTON) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_product_image_holder, viewGroup, false);
            return new HomePickerViewHolder(v);
        } else if (viewType == GoogleOrderBot_ChatGlobal.STORE_SHOW) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_product_image_holder, viewGroup, false);
            return new StorePickerViewHolder(v);
        } else if (viewType == GoogleOrderBot_ChatGlobal.LOCATION_SHOW) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_location_holder, viewGroup, false);
            return new LocationShowViewHolder(v);
        } else {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.googlechatbot_chat_user_receive, viewGroup, false);
            return new ReceiveViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        if (viewHolder.getItemViewType() == GoogleOrderBot_ChatGlobal.SEND) {
            SendViewHolder holder = (SendViewHolder) viewHolder;
            holder.sendMessage.setText(chatDataModelList.get(position).getmDataset());
            holder.time.setText(chatDataModelList.get(position).getmTime());
        } else if (viewHolder.getItemViewType() == GoogleOrderBot_ChatGlobal.RECEIVE_PRODUCT_IMAGE) {
            ReceiveProductImageViewHolder holder = (ReceiveProductImageViewHolder) viewHolder;
            GoogleOrderBot_ProductImageAdapter productImageAdapter = new GoogleOrderBot_ProductImageAdapter(mContext, chatDataModelList.get(position).getmProductImageArray());
            holder.productRecyclerView.setHasFixedSize(true);
            holder.productRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.productRecyclerView.setAdapter(productImageAdapter);
        } else if (viewHolder.getItemViewType() == GoogleOrderBot_ChatGlobal.RECEIVE_OFFER) {
            RecieveRecommendationViewHolder holder = (RecieveRecommendationViewHolder) viewHolder;
            GoogleOrderBot_RecommendationAdapter recommendationAdapter = new GoogleOrderBot_RecommendationAdapter(mContext, chatDataModelList.get(position).getRecommendArray());
            holder.recommendRecyclerView.setHasFixedSize(true);
            holder.recommendRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            holder.recommendRecyclerView.setAdapter(recommendationAdapter);
        } else if (viewHolder.getItemViewType() == GoogleOrderBot_ChatGlobal.RECIEVE_HOME_PICKER_BUTTON) {
            HomePickerViewHolder holder = (HomePickerViewHolder) viewHolder;
            GoogleOrderBot_HomePickerAdapter homePickerAdapter = new GoogleOrderBot_HomePickerAdapter(mContext, chatDataModelList.get(position).getHomepickerArray());
            holder.homepickerRecyclerView.setHasFixedSize(true);
            holder.homepickerRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            holder.homepickerRecyclerView.setAdapter(homePickerAdapter);
        } else if (viewHolder.getItemViewType() == GoogleOrderBot_ChatGlobal.STORE_SHOW) {
            StorePickerViewHolder holder = (StorePickerViewHolder) viewHolder;
            GoogleOrderBot_StorePickerAdapter storePickerAdapter = new GoogleOrderBot_StorePickerAdapter(mContext, chatDataModelList.get(position).getStorpickerArray());
            holder.storepickerRecyclerView.setHasFixedSize(true);
            holder.storepickerRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            holder.storepickerRecyclerView.setAdapter(storePickerAdapter);
        } else if (viewHolder.getItemViewType() == GoogleOrderBot_ChatGlobal.LOCATION_SHOW) {
            LocationShowViewHolder holder = (LocationShowViewHolder) viewHolder;
            GoogleOrderBot_LocationAdapter locationAdapter = new GoogleOrderBot_LocationAdapter(mContext, chatDataModelList.get(position).getSingleLocationarray());
            holder.locationRecyclerView.setHasFixedSize(true);
            holder.locationRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            holder.locationRecyclerView.setAdapter(locationAdapter);
        } else {
            ReceiveViewHolder holder = (ReceiveViewHolder) viewHolder;
            holder.receiveMessage.setText(chatDataModelList.get(position).getmDataset());
            holder.time.setText(chatDataModelList.get(position).getmTime());
        }
    }

    @Override
    public int getItemCount() {

        return chatDataModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
//
        return chatDataModelList.get(position).getmDatasetTypes();
    }


}
