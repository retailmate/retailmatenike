package com.cognizant.iot.googlebot.orderbot;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;


import com.cognizant.iot.googlebot.orderbot.adapter.GoogleOrderBot_ChatAdapter;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_ChatDataModel;

import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_HomePicker;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_RecommendModel;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_SingleLocation;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_SingleProduct;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_StorePicker;
import com.cognizant.retailmate.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.JsonElement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ai.api.AIServiceException;
import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;

public class GoogleOrderBot_ChatMain extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    GoogleOrderBot_ChatGlobal chatGlobal;
    GoogleOrderBot_ChatUtil chatUtil;
    private static final String CustomTag = "CustomTag";
    private String userSays = null;
    String aistorename_update, aiselectiontime_update, ailockername_update, speech;
    public static String intent;
    public static String aistore_update;
    public static String ailocker_update;
    String purchasehistoryapi = "http://jdaretailmateapidev.azurewebsites.net/api/PurchaseServices/GetUserPurchaseHistoryAPI";
    String recommendationapi = "http://jdaretailmateapidev.azurewebsites.net/api/CognitiveServices/GetUserToItemRecommendationsAPI";
    HashMap<String, JsonElement> aijson;
    RequestQueue queue;
    public static String imageparentlink = "https://JDARetailMatedevret.cloudax.dynamics.com/MediaServer/";
    int itemfound = 0;
    //RecyclerView productimageholder;
    //RecyclerView.LayoutManager imagelayoutmanager;
    JSONObject finalobject = null;
    JSONObject recommendobject = null;
    public static ArrayList<GoogleOrderBot_SingleProduct> imageurl = null;
    public static ArrayList<GoogleOrderBot_HomePicker> homepickerlist;
    //public static ArrayList<GoogleOrderBot_RecommendModel> recommendurl=null;
    double rewardpoint = 200;
    //AIResponse apiairesponse;
    double latitude = 12.824, longtitude = 80.221;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    public static LatLngBounds current_place;
    public static int PLACE_PICKER_REQUEST = 1;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int userYear, userMonth, userDay, userHour, userMinute;
    TimePickerDialog timePickerDialog;
    DatePickerDialog datePickerDialog;
    String previous_date_time = "15:29 15-1-2017";
    String timedatechangeparameter;
    String entity;
    AIRequest aiRequest;
    AIAsyncTask aiAsyncTask;
    JSONObject aistore = null;
    JSONObject onlytimedatechange = null;
    ArrayList<GoogleOrderBot_SingleLocation> singlelocationlist;
    ArrayList<GoogleOrderBot_StorePicker> storepickerlist;
    public static ArrayList<String> locationlist;
    public static ArrayList<String> lockerlist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.googlechatbot_activity_chat_main);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        chatGlobal = new GoogleOrderBot_ChatGlobal(getApplicationContext());
        queue = Volley.newRequestQueue(this);
        chatUtil = new GoogleOrderBot_ChatUtil();
        GoogleOrderBot_ChatGlobal.chatDataModels = new ArrayList<>();
        GoogleOrderBot_ChatGlobal.sendButton = (ImageButton) findViewById(R.id.enter_chat1);
        GoogleOrderBot_ChatGlobal.mDatasetTypes = new ArrayList<Integer>();
        GoogleOrderBot_ChatGlobal.mDataset = new ArrayList<String>();
        GoogleOrderBot_ChatGlobal.mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        GoogleOrderBot_ChatGlobal.mLayoutManager = new LinearLayoutManager(GoogleOrderBot_ChatMain.this);
        GoogleOrderBot_ChatGlobal.mRecyclerView.setLayoutManager(GoogleOrderBot_ChatGlobal.mLayoutManager);
        //Adapter is created in the last step
        GoogleOrderBot_ChatGlobal.mAdapter = new GoogleOrderBot_ChatAdapter(GoogleOrderBot_ChatMain.this, GoogleOrderBot_ChatGlobal.chatDataModels);
        GoogleOrderBot_ChatGlobal.mRecyclerView.setAdapter(GoogleOrderBot_ChatGlobal.mAdapter);
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size() - 1);
        GoogleOrderBot_ChatGlobal.chatText = (EditText) findViewById(R.id.chat_edit_text);
        //getting user location
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        //

        timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        userHour = hourOfDay;
                        userMinute = minute;
                        if (userHour == 0) {
                            GoogleOrderBot_ChatGlobal.chatText.setText(userDay + "-" + (userMonth + 1) + "-" + userYear + " at 12" + ":" + userMinute + "am");
                        } else if (userHour == 12) {
                            GoogleOrderBot_ChatGlobal.chatText.setText(userDay + "-" + (userMonth + 1) + "-" + userYear + " at 12" + ":" + userMinute + "pm");
                        } else if (userHour < 12) {
                            GoogleOrderBot_ChatGlobal.chatText.setText(userDay + "-" + (userMonth + 1) + "-" + userYear + " at " + userHour + ":" + userMinute + "am");
                        } else {
                            GoogleOrderBot_ChatGlobal.chatText.setText(userDay + "-" + (userMonth + 1) + "-" + userYear + " at " + (userHour - 12) + ":" + userMinute + "pm");
                        }
                        GoogleOrderBot_ChatGlobal.sendButton.callOnClick();
                    }
                }, mHour, mMinute, false);
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                userYear = year;
                userMonth = month;
                userDay = dayOfMonth;
                timePickerDialog.show();
            }
        }, mYear, mMonth, mDay);
        chatUtil.setReplyMessage("Hi! How can i help you with your order");
        GoogleOrderBot_ChatGlobal.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userSays = GoogleOrderBot_ChatGlobal.chatText.getText().toString();
                GoogleOrderBot_ChatGlobal.chatText.setText("");
                aiRequest = new AIRequest();
                aiRequest.setQuery(userSays);
                aiAsyncTask = new AIAsyncTask();
                aiAsyncTask.execute(aiRequest);
                chatUtil.setSendMessage(userSays);

                //hide keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(GoogleOrderBot_ChatGlobal.sendButton.getApplicationWindowToken(), 0);
            }
        });

        GoogleOrderBot_ChatGlobal.mRecyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override

            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size() - 1);
            }
        });
    }

    class AIAsyncTask extends AsyncTask<AIRequest, Void, AIResponse> {   //AICHATBOT framework configuration

        @Override
        protected AIResponse doInBackground(AIRequest... requests) {
            final AIRequest request = requests[0];

            final AIConfiguration config = new AIConfiguration(GoogleOrderBot_ChatGlobal.ACCESS_TOKEN,
                    AIConfiguration.SupportedLanguages.English,
                    AIConfiguration.RecognitionEngine.System);


            final AIDataService aiDataService = new AIDataService(chatGlobal.mContext, config);

            try {
                final AIResponse response = aiDataService.request(request);
                return response;
            } catch (AIServiceException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(AIResponse aiResponse) {       //Response which we are getting
            if (aiResponse != null) {
                // process aiResponse here
                Log.e("TAG", "Response = " + aiResponse.getResult().getFulfillment().getSpeech().toString());
                Log.e("TAG", "Intent = " + aiResponse.getResult().getMetadata().getIntentName().toString());
                speech = aiResponse.getResult().getFulfillment().getSpeech().toString();
                intent = aiResponse.getResult().getMetadata().getIntentName().toString();   //storing the intent name from ai
                aijson = aiResponse.getResult().getParameters();//getting the json with parameters from ai
                entity = aiResponse.getResult().getResolvedQuery();
                entity = entity.toLowerCase();
                //apiairesponse=aiResponse;
                Log.e("TAG", "actionIncomplete = " + aiResponse.getResult().isActionIncomplete());
                Log.e("TAG", "main result--->" + aiResponse.getResult());
                Log.e("TAG", "result--->" + aiResponse.getResult().getParameters());
                /*if(intent.equals("Only_update_date_time")) {
                    if (aijson.toString() != null) {
                        try {
                            onlytimedatechange = new JSONObject(aijson.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            timedatechangeparameter = onlytimedatechange.getString("Update_date_time");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        timedatechangeparameter = timedatechangeparameter.toLowerCase();
                        if (timedatechangeparameter.contains("change") || timedatechangeparameter.contains("update")) {
                            datePickerDialog.show();
                        }
                    }
                }*/
                if (intent.equals("Update Address") && speech.equals("I need to know the new delivery address")) {
                    GoogleOrderBot_ChatUtil.setReplyMessage(speech);
                    startshowinghomepicker();
                } else if (intent.equals("Update date and time") && entity.equals("yes") || entity.equals("yup") || entity.equals("yeah") || entity.equals("ok") || entity.equals("go ahead! i want to change") || entity.equals("i do") || entity.equals("yes i do") || entity.equals("yes! i do")) {
                    datePickerDialog.show();
                    //GoogleOrderBot_ChatUtil.setReplyMessage("Your delivery address and preferred date and time for delivery has been updated accordingly and your product will reach you at your newly mentioned address and date time!");
                } else if (intent.equals("Update date and time") && entity.equals("no") || entity.equals("nope") || entity.equals("not required") || entity.equals("no need") || entity.equals("not now") || entity.equals("keep it same") || entity.equals("keep that as it is") || entity.equals("keep it as it it")) {
                    GoogleOrderBot_ChatUtil.setReplyMessage("Ok! Your product will reach your newly mentioned address on your preferred date and time");
                } else {
                    if (aijson.toString() != null) {
                        try {
                            aistore = new JSONObject(aijson.toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            aistore_update = aistore.getString("update_store");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            ailocker_update = aistore.getString("update_locker");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (intent.equals("StoreUpdate") && aistore_update != null) {
                        try {
                            aistorename_update = aistore.getString("store_name");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            aiselectiontime_update = aistore.getString("selection_time");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (aistorename_update == null && aiselectiontime_update == null) {
                            Log.e("1 store name " + aistorename_update, "selection time " + aiselectiontime_update);
                            chatUtil.setReplyMessage(aiResponse.getResult().getFulfillment().getSpeech());
                            try {
                                parselocationjson();
                                showstorepicker();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //showstorepicker();
                        }
                        if (aistorename_update != null && aiselectiontime_update == null) {
                            Log.e("2 store name " + aistorename_update, "selection time " + aiselectiontime_update);
                            chatUtil.setReplyMessage(aiResponse.getResult().getFulfillment().getSpeech());
                        }
                        if (aistorename_update != null && aiselectiontime_update != null) {
                            if (aiselectiontime_update.equals("yes")) {
                                Log.e("3 store name " + aistorename_update, "selection time " + aiselectiontime_update);
                                datePickerDialog.show();
                                aistorename_update = null;
                                aiselectiontime_update = null;
                            } else {
                                Log.e("4 store name " + aistorename_update, "selection time " + aiselectiontime_update);
                                chatUtil.setReplyMessage("Store is updated according to your choice");
                                aistorename_update = null;
                                aiselectiontime_update = null;
                            }
                        }
                    } else if (intent.equals("LockerUpdate") && ailocker_update != null) {
                        try {
                            ailockername_update = aistore.getString("locker_name");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            aiselectiontime_update = aistore.getString("selection_time");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (ailockername_update == null && aiselectiontime_update == null) {
                            Log.e("1 store name " + ailockername_update, "selection time " + aiselectiontime_update);
                            chatUtil.setReplyMessage(aiResponse.getResult().getFulfillment().getSpeech());
                            try {
                                parselockerjson();
                                showstorepicker();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //showstorepicker();
                        }
                        if (ailockername_update != null && aiselectiontime_update == null) {
                            Log.e("2 store name " + ailockername_update, "selection time " + aiselectiontime_update);
                            chatUtil.setReplyMessage(aiResponse.getResult().getFulfillment().getSpeech());
                        }
                        if (ailockername_update != null && aiselectiontime_update != null) {
                            if (aiselectiontime_update.equals("yes")) {
                                Log.e("3 store name " + ailockername_update, "selection time " + aiselectiontime_update);
                                datePickerDialog.show();
                                ailockername_update = null;
                                aiselectiontime_update = null;
                            } else {
                                Log.e("4 store name " + ailockername_update, "selection time " + aiselectiontime_update);
                                chatUtil.setReplyMessage("Locker is updated according to your choice");
                                ailockername_update = null;
                                aiselectiontime_update = null;
                            }
                        }
                    } else
                        chatUtil.setReplyMessage(aiResponse.getResult().getFulfillment().getSpeech());
                }
            }
        }
    }


    private void showstorepicker() {
        storepickerlist = new ArrayList<GoogleOrderBot_StorePicker>();
        final GoogleOrderBot_StorePicker storepicker = new GoogleOrderBot_StorePicker();
        if (intent.equals("StoreUpdate")) {
            storepicker.setStoreimageid(R.drawable.store);
        } else {
            storepicker.setStoreimageid(R.drawable.locker);
        }
        storepickerlist.add(storepicker);
        GoogleOrderBot_ChatDataModel storepickerrequest = new GoogleOrderBot_ChatDataModel();
        storepickerrequest.setmDatasetTypes(GoogleOrderBot_ChatGlobal.STORE_SHOW);
        setGoogleOrderBot_StorePicker(storepickerlist, storepickerrequest);
    }

    private void parselockerjson() throws JSONException {
        lockerlist = new ArrayList<String>();
        try {
            JSONObject object = new JSONObject(loadLockerJSONFromAsset());
            JSONArray locationarray = object.getJSONArray("locations");
            for (int i = 0; i < locationarray.length(); i++) {
                JSONObject locationobject = locationarray.getJSONObject(i);
                GoogleOrderBot_SingleLocation location = new GoogleOrderBot_SingleLocation();
                location.setLocation_Name(locationobject.getString("place"));
                Log.e("location", locationobject.getString("place"));
                location.setLocation_Address(locationobject.getString("address"));
                Log.e("Address", locationobject.getString("address"));
                //singlelocationlist.add(location);
                lockerlist.add(locationobject.getString("place"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Log.e("json",""+singlelocationlist.toString());
    }


    private void parselocationjson() throws JSONException {
        singlelocationlist = new ArrayList<GoogleOrderBot_SingleLocation>();
        locationlist = new ArrayList<String>();
        //locationlist.add("");
        try {
            JSONObject object = new JSONObject(loadLocationJSONFromAsset());
            JSONArray locationarray = object.getJSONArray("locations");
            for (int i = 0; i < locationarray.length(); i++) {
                JSONObject locationobject = locationarray.getJSONObject(i);
                GoogleOrderBot_SingleLocation location = new GoogleOrderBot_SingleLocation();
                location.setLocation_Name(locationobject.getString("place"));
                Log.e("location", locationobject.getString("place"));
                location.setLocation_Address(locationobject.getString("address"));
                Log.e("Address", locationobject.getString("address"));
                singlelocationlist.add(location);
                locationlist.add(locationobject.getString("place"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("json", "" + singlelocationlist.toString());
        /*GoogleOrderBot_ChatDataModel locationrequest = new GoogleOrderBot_ChatDataModel();
        locationrequest.setmDatasetTypes(GoogleOrderBot_ChatGlobal.LOCATION_SHOW);
        setLocationrequest(singlelocationlist,locationrequest);*/

    }

    private String loadLockerJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open("lockers.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("JSONLOCKER", json);
        return json;
    }

    private String loadLocationJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getApplicationContext().getAssets().open("location.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        Log.e("JSONLOCATION", json);
        return json;
    }


    private void startshowinghomepicker() {                          //adapter calling for home picker button
        homepickerlist = new ArrayList<GoogleOrderBot_HomePicker>();
        final GoogleOrderBot_HomePicker homePicker = new GoogleOrderBot_HomePicker();
        homePicker.setImageid(R.drawable.home_picker);
        homepickerlist.add(homePicker);
        GoogleOrderBot_ChatDataModel homepickerRequest = new GoogleOrderBot_ChatDataModel();
        homepickerRequest.setmDatasetTypes(GoogleOrderBot_ChatGlobal.RECIEVE_HOME_PICKER_BUTTON);
        setGoogleOrderBot_HomePicker(homepickerlist, homepickerRequest);
    }


    /*public void startShowingMap()
    {
        try {
            PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
            intentBuilder.setLatLngBounds(current_place);
            Intent intent = intentBuilder.build(GoogleOrderBot_ChatMain.this);
            startActivityForResult(intent, PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }*/
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {   //result from placepicker
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                chatGlobal.chatText.setText(place.getAddress().toString());
            }
        }
    }

    private void parserecommendjson(String recommendationapi) {
        Map<String, String> jsonParamsrecommend = new HashMap<String, String>();
        jsonParamsrecommend.put("UserId", "004021");
        JsonObjectRequest recommendrequest = new JsonObjectRequest(Request.Method.POST, recommendationapi,

                new JSONObject(jsonParamsrecommend),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("PurchaseJson", response.toString());
                        //json=response.toString();
                        chatUtil.setReplyMessage("Here are the products according to your Loyalty points \n");
                        parserecommendapi(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Log.e("Volley error",error.getMessage());
                        chatUtil.setReplyMessage("I am finding some problem in connectivity!! Give me some time!!");
                        //   Handle Error
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> recommendheaders = new HashMap<String, String>();
                recommendheaders.put("Content-Type", "application/json; charset=utf-8");
                recommendheaders.put("User-agent", System.getProperty("http.agent"));
                return recommendheaders;
            }
        };
        queue.add(recommendrequest);
    }

    private void parserecommendapi(JSONObject response) {
        ArrayList<GoogleOrderBot_RecommendModel> recommendproductlist = new ArrayList<>();
        JSONArray parentarray = null;
        double price = 0;
        try {
            parentarray = response.getJSONArray("value");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < parentarray.length(); i++) {
            try {
                recommendobject = parentarray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            double custprice = 0;
            try {
                custprice = Double.parseDouble(recommendobject.getString("Custprice"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            price += custprice;
            if (price > rewardpoint) {
                break;
            } else {
                //recommendurl = preparerecommendurl();
                GoogleOrderBot_RecommendModel recommendproduct = new GoogleOrderBot_RecommendModel();
                try {
                    recommendproduct.setImageurl(recommendobject.getString("Image"));
                    recommendproduct.setProductname(recommendobject.getString("ProductName"));
                    recommendproduct.setAmount(recommendobject.getString("Custprice"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                recommendproductlist.add(recommendproduct);
            }
        }
        GoogleOrderBot_ChatDataModel recommendrequest = new GoogleOrderBot_ChatDataModel();
        recommendrequest.setmDatasetTypes(GoogleOrderBot_ChatGlobal.RECEIVE_OFFER);
        setrecommendation(recommendproductlist, recommendrequest);    //to set the recommendations
    }


    /*private void parseaijson(String ai) {     //AI json parsing
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(ai);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            aiprice=jsonObject.getString("cost");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            aiitem = jsonObject.getString("item");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            ainumber = jsonObject.getString("number");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            aiimagelink=jsonObject.getString("image");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("TAG", "details---->" + ainumber + aiprice + aiitem + aiimagelink);
        if (aiprice != null || ainumber != null || aiimagelink != null) {
            jsonrequest(purchasehistoryapi);
        }
    }*/


    private void jsonrequest(String purchasehistoryapi) {                           //api calling for purchase history
        Map<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("UserId", "004021");
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, purchasehistoryapi,

                new JSONObject(jsonParams),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("J", response.toString());
                        //json=response.toString();
                        //parsepurchase(response);                              //method to parse json got from api calling for purchase history
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Log.e("Volley error",error.getMessage());
                        chatUtil.setReplyMessage("I am finding some problem in connectivity!! Give me some time!!");
                        //   Handle Error
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("User-agent", System.getProperty("http.agent"));
                return headers;
            }
        };
        queue.add(postRequest);
    }

    /*private void parsepurchase(JSONObject response){                //parsing the json related to purchase history
        Log.e("///",response.toString());
        JSONArray parentArray= null;
        try {
            parentArray = response.getJSONArray("value");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for(int i=0;i<parentArray.length();i++)
        {
            //JSONObject finalobject= null;
            try {
                finalobject = parentArray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                if(finalobject.getString("ProductName").toLowerCase().contains(aiitem))
                {
                    if(ainumber!=null)
                    {
                        chatUtil.setReplyMessage("The quantity of "+aiitem+" you bought last time is "+finalobject.getString("Qty"));
                        ainumber=null;
                    }
                    if(aiprice!=null)
                    {
                        chatUtil.setReplyMessage("The amount you spent on "+aiitem+" is "+finalobject.getString("UnitPrice"));
                        aiprice=null;
                    }
                    if(aiimagelink!=null)
                    {
                        chatUtil.setReplyMessage("Let me show you your product");
                        imageurl=prepareimageurl();
                        GoogleOrderBot_ChatDataModel chatimagerequest=new GoogleOrderBot_ChatDataModel();
                        chatimagerequest.setmDatasetTypes(GoogleOrderBot_ChatGlobal.RECEIVE_PRODUCT_IMAGE);
                        //GoogleOrderBot_ChatGlobal.chatDataModels.add(chatimagerequest);
                        setproductimage(imageurl,chatimagerequest);
                        aiimagelink=null;

                    }
                    aiitem=null;   //not to retain the item name from previous query
                    itemfound=1;
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(itemfound==0)
        {
            chatUtil.setReplyMessage("We cannot find the item you just mentioned in your purchase history");
        }
        itemfound=0;
    }*/

    /*private ArrayList<GoogleOrderBot_RecommendModel> preparerecommendurl() {
        ArrayList<GoogleOrderBot_RecommendModel> recommendurllist=new ArrayList<>();
        GoogleOrderBot_RecommendModel recommendproduct=new GoogleOrderBot_RecommendModel();
        try {
            recommendproduct.setImageurl(recommendobject.getString("Image"));
            recommendproduct.setProductname(recommendobject.getString("ProductName"));
            recommendproduct.setAmount(recommendobject.getString("Custprice"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        recommendurllist.add(recommendproduct);
        return recommendurllist;
    }*/

    private ArrayList<GoogleOrderBot_SingleProduct> prepareimageurl() {
        ArrayList<GoogleOrderBot_SingleProduct> imageurllist = new ArrayList<>();
        GoogleOrderBot_SingleProduct singleproduct = new GoogleOrderBot_SingleProduct();
        try {
            singleproduct.setImageurl(finalobject.getString("Image"));
            singleproduct.setProductname(finalobject.getString("ProductName"));
            singleproduct.setAmount(finalobject.getString("UnitPrice"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        imageurllist.add(singleproduct);
        return imageurllist;
    }

    public void setproductimage(ArrayList<GoogleOrderBot_SingleProduct> urllist, GoogleOrderBot_ChatDataModel detailrequest) {
        detailrequest.setmProductImageArray(urllist);
        GoogleOrderBot_ChatGlobal.chatDataModels.add(detailrequest);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size() - 1);
    }

    private void setrecommendation(ArrayList<GoogleOrderBot_RecommendModel> recommendproduct, GoogleOrderBot_ChatDataModel recommendrequest) {
        recommendrequest.setRecommendArray(recommendproduct);
        GoogleOrderBot_ChatGlobal.chatDataModels.add(recommendrequest);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size() - 1);
    }

    private void setGoogleOrderBot_HomePicker(ArrayList<GoogleOrderBot_HomePicker> homepickerlist, GoogleOrderBot_ChatDataModel homepickerRequest) {
        homepickerRequest.setHomepickerArray(homepickerlist);
        GoogleOrderBot_ChatGlobal.chatDataModels.add(homepickerRequest);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size() - 1);
    }

    private void setGoogleOrderBot_StorePicker(ArrayList<GoogleOrderBot_StorePicker> storepickerlist, GoogleOrderBot_ChatDataModel storepickerrequest) {
        storepickerrequest.setStorpickerArray(storepickerlist);
        GoogleOrderBot_ChatGlobal.chatDataModels.add(storepickerrequest);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size() - 1);
    }

    private void setLocationrequest(ArrayList<GoogleOrderBot_SingleLocation> singlelocationlist, GoogleOrderBot_ChatDataModel locationrequest) {
        locationrequest.setSingleLocationarray(singlelocationlist);
        Log.e("list", singlelocationlist.get(0).getLocation_Name().toString());
        GoogleOrderBot_ChatGlobal.chatDataModels.add(locationrequest);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size() - 1);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("TAG", "IN");
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longtitude = mLastLocation.getLongitude();
            Log.e("PLACE", "LOCATION" + latitude + " " + longtitude);
            current_place = new LatLngBounds(
                    new LatLng(latitude - 0.008, longtitude - 0.014), new LatLng(latitude + 0.015, longtitude + 0.011));
        } else {
            Log.e("LOCATION", "DEFAULT");
            current_place = new LatLngBounds(
                    new LatLng(latitude - 0.008, longtitude - 0.014), new LatLng(latitude + 0.015, longtitude + 0.011));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getApplicationContext(), "Connection failed!!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.aboutusmenu, menu);
        // setMenuBackground();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.mail_aboutus:
                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] recipients = {"retailmate_customersupport@gmail.com"};
                intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Retail Mate Subject");
                intent.putExtra(Intent.EXTRA_TEXT, "Retail Mate Sample message");
                intent.putExtra(Intent.EXTRA_CC, "retailmate_admin@gmail.com");
                intent.setType("text/html");
                startActivity(Intent.createChooser(intent, "Send mail"));
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
