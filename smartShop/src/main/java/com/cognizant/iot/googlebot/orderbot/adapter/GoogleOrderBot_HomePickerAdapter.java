package com.cognizant.iot.googlebot.orderbot.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cognizant.iot.googlebot.orderbot.GoogleOrderBot_ChatMain;
import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_HomePicker;

import com.cognizant.retailmate.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.ArrayList;
import java.util.List;

import static com.cognizant.iot.googlebot.orderbot.GoogleOrderBot_ChatMain.current_place;



/**
 * Created by Guest_User on 19/01/17.
 */

public class GoogleOrderBot_HomePickerAdapter extends RecyclerView.Adapter<GoogleOrderBot_HomePickerAdapter.HomeViewHolder> {
    private Context context;
    private List<GoogleOrderBot_HomePicker> homepickerlist;


    public GoogleOrderBot_HomePickerAdapter(Context context, ArrayList<GoogleOrderBot_HomePicker> homepickerlist) {
        this.homepickerlist = homepickerlist;
        this.context = context;
    }

    @Override
    public HomeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.googlechatbot_home_picker_button, parent, false);
        HomeViewHolder homeViewHolder = new HomeViewHolder(view);
        return homeViewHolder;
    }

    @Override
    public void onBindViewHolder(HomeViewHolder holder, int position) {
        final GoogleOrderBot_HomePicker homePicker = homepickerlist.get(position);
        holder.home_picker.setImageResource(homePicker.getImageid());
    }

    @Override
    public int getItemCount() {
        return homepickerlist.size();
    }

    protected class HomeViewHolder extends RecyclerView.ViewHolder {

        ImageView home_picker;
        int startmap = 0;
        Activity activity;
        PlacePicker.IntentBuilder intentBuilder;

        public HomeViewHolder(View itemView) {
            super(itemView);
            home_picker = (ImageView) itemView.findViewById(R.id.homepicker);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GoogleOrderBot_ChatMain chatmain = new GoogleOrderBot_ChatMain();
                    activity = chatmain.getParent();
                    try {
                        activity = (Activity) context;
                        intentBuilder = new PlacePicker.IntentBuilder();
                        intentBuilder.setLatLngBounds(current_place);
                        Intent intent = intentBuilder.build(activity);
                        ((Activity) context).startActivityForResult(intent, GoogleOrderBot_ChatMain.PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }
                }
            });
        }


        /*if(startmap==1){
            ((GoogleOrderBot_ChatMain)getClass()).startShowingMap();
        }*/
    }
}
