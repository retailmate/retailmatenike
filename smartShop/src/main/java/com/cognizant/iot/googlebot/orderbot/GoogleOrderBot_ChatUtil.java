package com.cognizant.iot.googlebot.orderbot;

import android.text.format.DateFormat;


import com.cognizant.iot.googlebot.orderbot.model.GoogleOrderBot_ChatDataModel;

import java.util.Date;


/**
 * Created by 540472 on 12/27/2016.
 */
public class GoogleOrderBot_ChatUtil {

    private static final String CustomTag = "CustomTag";
/*

    public String loadJSONFromAsset(String fileName) {
        String json = null;
        try {
            InputStream is = GoogleOrderBot_ChatGlobal.mContext.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void clearList(){
        GoogleOrderBot_ChatGlobal.entityItemList.clear();
        GoogleOrderBot_ChatGlobal.entityBooleanList.clear();
        GoogleOrderBot_ChatGlobal.entityNumberList.clear();
        GoogleOrderBot_ChatGlobal.itemMatchedList.clear();
        GoogleOrderBot_ChatGlobal.itemMatchedIdList.clear();
        GoogleOrderBot_ChatGlobal.productList.clear();
    }

    public String stringToGoogleSearchUrl(String baseURL, String userSays, String endUrl) {
        String convertedString=null;
        StringBuffer searchbuffer=new StringBuffer(baseURL);
        Log.e(CustomTag,"usersaid = "+userSays);
        StringTokenizer st = new StringTokenizer(userSays," ");
        int length= st.countTokens();
        for (int i=0;i<length-1;i++){
            searchbuffer.append(st.nextToken());
            searchbuffer.append("+");
        }
        searchbuffer.append(st.nextToken());
        convertedString=searchbuffer.toString();
        convertedString=convertedString+endUrl;
        return convertedString;
    }

    //For setting suggestion texts.
    public void setSuggestDataModels(){
        SuggestDataModel suggestDataModel = new SuggestDataModel();
        suggestDataModel.setSuggestion("Do you have any offers ?");
        Log.e("TAG", "#### setSuggestion" + suggestDataModel.getSuggestion());
        GoogleOrderBot_ChatGlobal.suggestDataModels.add(suggestDataModel);
        SuggestDataModel suggestDataModel1 = new SuggestDataModel();
        suggestDataModel1.setSuggestion("Recommend me some products");
        Log.e("TAG", "#### setSuggestion" + suggestDataModel.getSuggestion());
        GoogleOrderBot_ChatGlobal.suggestDataModels.add(suggestDataModel1);

        SuggestDataModel suggestDataModel2 = new SuggestDataModel();
        suggestDataModel2.setSuggestion("Add to cart");
        Log.e("TAG", "#### setSuggestion" + suggestDataModel.getSuggestion());
        GoogleOrderBot_ChatGlobal.suggestDataModels.add(suggestDataModel2);

        SuggestDataModel suggestDataModel3 = new SuggestDataModel();
        suggestDataModel3.setSuggestion("Checkout Cart");
        Log.e("TAG", "#### setSuggestion" + suggestDataModel.getSuggestion());
        GoogleOrderBot_ChatGlobal.suggestDataModels.add(suggestDataModel3);
    }

    //For URL of LUIS
    public String stringToUrl(String baseURL, String userSays) {
        String convertedString=null;
        StringBuffer sb=new StringBuffer(baseURL);

        StringTokenizer st = new StringTokenizer(userSays," ");
        while (st.hasMoreTokens()) {
            sb.append(st.nextToken());
            sb.append("%20");
        }
        convertedString=sb.toString();

        return convertedString;
    }

    public String stringToUrlContextId(String baseURL, String userSays, String contextId) {
        String convertedString=null;
        StringBuffer sb=new StringBuffer(baseURL);

        StringTokenizer st = new StringTokenizer(userSays," ");
        while (st.hasMoreTokens()) {
            sb.append(st.nextToken());
            sb.append("%20");
        }
        sb.append("&contextId="+contextId);
        convertedString=sb.toString();

        return convertedString;
    }

    public String makeProductSearchApi(String userSays){
        String api="";
        StringBuffer sb = new StringBuffer(GoogleOrderBot_ChatGlobal.baseProductSearchAPI);

        StringTokenizer st = new StringTokenizer(userSays, " ");
        while (st.hasMoreTokens()) {
            sb.append(st.nextToken());
            sb.append("%20");
        }
        sb.append("%27)?%24top=20&api-version=7.1");
        api = sb.toString();
        Log.e(CustomTag,"Api = "+api);
        return  api;
    }

    public void populateEntityList(JSONArray entityArray){
        Log.d(CustomTag,"entityArray.length() = "+entityArray.length());
        for (int i=0;i<entityArray.length();i++){
            JSONObject entityObj = null;
            try {
                entityObj = entityArray.getJSONObject(i);
                String type = entityObj.getString("type");
                if (type.equalsIgnoreCase("item")) {
                    String entity = entityObj.getString("entity").toLowerCase();
                    GoogleOrderBot_ChatGlobal.entityItemList.add(entity);
                    Log.d(CustomTag,"item = "+entity);
                }
                if (type.equalsIgnoreCase("boolean")) {
                    String entity = entityObj.getString("entity").toLowerCase();
                    GoogleOrderBot_ChatGlobal.entityBooleanList.add(entity);
                    Log.d(CustomTag,"boolean = "+entity);
                }
                if (type.equalsIgnoreCase("builtin.number")) {
                    //int number = entityObj.getInt("entity");
                    String number = entityObj.getString("entity").toLowerCase();
                    StringTokenizer tokenizer =new StringTokenizer(number," - ");
                    while(tokenizer.hasMoreTokens()){
                        int numb= Integer.parseInt(tokenizer.nextToken());
                        GoogleOrderBot_ChatGlobal.entityNumberList.add(numb);
                    }
                    Log.d(CustomTag,"number = "+number);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void prompt(JSONObject dialogObj){
        GoogleOrderBot_ChatGlobal.isPrompt=true;
        String prompt = null;
        try {
            prompt = dialogObj.getString("prompt");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String reply = prompt;
        setReplyMessage(reply);
        Log.d(CustomTag, "prompt = " + prompt);

    }

*/


    /* @SuppressWarnings("deprecation")
     private static void ttsUnder20(String text) {
         HashMap<String, String> map = new HashMap<>();
         map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
         GoogleOrderBot_ChatGlobal.textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, map);
     }

     @TargetApi(Build.VERSION_CODES.LOLLIPOP)
     private static void ttsGreater21(String text) {
         String utteranceId=GoogleOrderBot_ChatGlobal.mContext.hashCode() + "";
         GoogleOrderBot_ChatGlobal.textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
     }
 */
   /* public void setReplySearch(List<SearchDataModel> productList){

        GoogleOrderBot_ChatDataModel chatReceiveData=new GoogleOrderBot_ChatDataModel();
        ArrayList<SearchDataModel> searchDataModelArrayList=new ArrayList<>();
        chatReceiveData.setmDatasetTypes(GoogleOrderBot_ChatGlobal.RECEIVE_SEARCH);
        Date d1 = new Date();
        String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
        chatReceiveData.setmTime(time1);
        for (int i = 0; i <productList.size() ; i++) {

            SearchDataModel searchDataModel=new SearchDataModel();
            searchDataModel.setTitle(productList.get(i).getTitle());
            searchDataModel.setSnippet(productList.get(i).getSnippet());
            searchDataModel.setLink(productList.get(i).getLink());
            if (GoogleOrderBot_ChatGlobal.hasSearchImage){
                searchDataModel.setImgURL(productList.get(i).getImgURL());
            }
            searchDataModelArrayList.add(searchDataModel);
        }
        chatReceiveData.setmSearchArray(searchDataModelArrayList);
        GoogleOrderBot_ChatGlobal.chatDataModels.add(chatReceiveData);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size()-1);
    }
*/
    //Used for getting resource id of offer images
    public int getResourceId(String pVariableName, String pResourcename, String pPackageName) {
        try {
            return GoogleOrderBot_ChatGlobal.mContext.getResources().getIdentifier(pVariableName, pResourcename, pPackageName);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static void setReplyMessage(String reply) {
        GoogleOrderBot_ChatDataModel chatSendData = new GoogleOrderBot_ChatDataModel();
        chatSendData.setmDataset(reply);
        chatSendData.setmDatasetTypes(GoogleOrderBot_ChatGlobal.RECEIVE);
        Date d = new Date();
        String time = (String) DateFormat.format("HH:mm", d.getTime());
        chatSendData.setmTime(time);
        GoogleOrderBot_ChatGlobal.chatDataModels.add(chatSendData);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size() - 1);

    }

    public void setSendMessage(String userSays) {
        GoogleOrderBot_ChatDataModel chatSendData = new GoogleOrderBot_ChatDataModel();
        chatSendData.setmDataset(userSays);
        chatSendData.setmDatasetTypes(GoogleOrderBot_ChatGlobal.SEND);
        Date d = new Date();
        String time = (String) DateFormat.format("HH:mm", d.getTime());
        chatSendData.setmTime(time);
        GoogleOrderBot_ChatGlobal.chatDataModels.add(chatSendData);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size() - 1);
    }


    /*public void setReplyProduct(List<ProductDataModel> productList){
        GoogleOrderBot_ChatDataModel chatReceiveData=new GoogleOrderBot_ChatDataModel();
        ArrayList<ProductDataModel> productDataModelArrayList=new ArrayList<>();
        chatReceiveData.setmDatasetTypes(GoogleOrderBot_ChatGlobal.RECEIVE_PRODUCT);
        Date d1 = new Date();
        String time1 = (String) DateFormat.format("HH:mm", d1.getTime());
        chatReceiveData.setmTime(time1);
        for (int i = 0; i <productList.size() ; i++) {
            ProductDataModel productDataModel=new ProductDataModel();
            productDataModel.setProductName(productList.get(i).getProductName());
            productDataModel.setPrice(productList.get(i).getPrice());
            productDataModel.setProductImageResource(productList.get(i).getProductImageResource());
            productDataModel.setProductId(productList.get(i).getProductId());
            productDataModelArrayList.add(productDataModel);
        }
        chatReceiveData.setmProductArray(productDataModelArrayList);
        GoogleOrderBot_ChatGlobal.chatDataModels.add(chatReceiveData);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size()-1);
    }

    public void setSuggestion(List<SuggestDataModel> suggestList) {
        Log.e("TAG", "#### setSuggestion");
        GoogleOrderBot_ChatDataModel chatSuggestData = new GoogleOrderBot_ChatDataModel();
        ArrayList<SuggestDataModel> suggestDataModelArrayList = new ArrayList<>();
        chatSuggestData.setmDatasetTypes(GoogleOrderBot_ChatGlobal.RECEIVE_SUGGEST);
        for (int i = 0; i < suggestList.size(); i++) {
            SuggestDataModel suggestDataModel = new SuggestDataModel();
            suggestDataModel.setSuggestion(suggestList.get(i).getSuggestion());
            suggestDataModelArrayList.add(suggestDataModel);
        }
        System.out.println("#### suggestDataModelArrayList" + suggestDataModelArrayList.get(0).getSuggestion());
        chatSuggestData.setmSuggestArray(suggestDataModelArrayList);
        GoogleOrderBot_ChatGlobal.chatDataModels.add(chatSuggestData);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size() - 1);
    }

    public void setReplyOffer(ArrayList<OfferDataModel> offerDataModelArrayList, GoogleOrderBot_ChatDataModel chatOfferData){
        chatOfferData.setmOfferArray(offerDataModelArrayList);
        GoogleOrderBot_ChatGlobal.chatDataModels.add(chatOfferData);
        GoogleOrderBot_ChatGlobal.mAdapter.notifyDataSetChanged();
        GoogleOrderBot_ChatGlobal.mRecyclerView.scrollToPosition(GoogleOrderBot_ChatGlobal.chatDataModels.size()-1);
    }
*/
}
