package com.cognizant.iot.googlebot.historyandloyaltybot.model;

import java.util.ArrayList;

/**
 * Created by 543898 on 10/18/2016.
 */
public class GoogleBot_ChatDataModel {
    String mDataset;
    String mTime;
    Integer mDatasetTypes;
    ArrayList<GoogleBot_RecommendModel> recommendArray;
    ArrayList<GoogleBot_SingleProduct> mProductImageArray;
    ArrayList<GoogleBot_HomePicker> homepickerArray;
    ArrayList<GoogleBot_LoyaltyBanner> loyaltyBannerArray;

    public ArrayList<GoogleBot_LoyaltyBanner> getLoyaltyBannerArray() {
        return loyaltyBannerArray;
    }

    public void setLoyaltyBannerArray(ArrayList<GoogleBot_LoyaltyBanner> loyaltyBannerArray) {
        this.loyaltyBannerArray = loyaltyBannerArray;
    }

    public ArrayList<GoogleBot_RecommendModel> getRecommendArray() {
        return recommendArray;
    }

    public void setRecommendArray(ArrayList<GoogleBot_RecommendModel> recommendArray) {
        this.recommendArray = recommendArray;
    }

    public ArrayList<GoogleBot_SingleProduct> getmProductImageArray() {
        return mProductImageArray;
    }

    public void setmProductImageArray(ArrayList<GoogleBot_SingleProduct> mProductImageArray) {
        this.mProductImageArray = mProductImageArray;
    }

    public ArrayList<GoogleBot_HomePicker> getHomepickerArray() {
        return homepickerArray;
    }

    public void setHomepickerArray(ArrayList<GoogleBot_HomePicker> homepickerArray) {
        this.homepickerArray = homepickerArray;
    }

    public String getmDataset() {
        return mDataset;
    }

    public void setmDataset(String mDataset) {
        this.mDataset = mDataset;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public Integer getmDatasetTypes() {
        return mDatasetTypes;
    }

    public void setmDatasetTypes(Integer mDatasetTypes) {
        this.mDatasetTypes = mDatasetTypes;
    }




}
