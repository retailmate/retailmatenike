package com.cognizant.iot.googlebot.historyandloyaltybot;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;


import com.cognizant.iot.googlebot.historyandloyaltybot.adapter.GoogleBot_ChatAdapter;
import com.cognizant.iot.googlebot.historyandloyaltybot.model.GoogleBot_ChatDataModel;

import java.util.ArrayList;
import java.util.List;

import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.model.AIRequest;

/**
 * Created by Guest_User on 29/12/16.
 */

public class GoogleBot_ChatGlobal {
    public static Context mContext;

    public GoogleBot_ChatGlobal(Context context) {
        this.mContext=context;
        Log.e("TAG","context = "+mContext);
    }

    public static String ACCESS_TOKEN="c06e2a3432704cb9ab3db7b1bb10fa4a";


    public static final int SEND = 0;
    public static final int RECEIVE = 1;
    public static final int RECEIVE_PRODUCT = 2;
    public static final int RECEIVE_OFFER = 3;
    public static final int RECEIVE_SEARCH = 4;
    public static final int RECEIVE_SUGGEST =5 ;
    public static final int RECEIVE_PRODUCT_IMAGE = 6;
    public static final int RECIEVE_HOME_PICKER_BUTTON=7;
    public static final int LOYALTY_POINTS_SHOW=8;

    //for YES/NO(means optional) response
    public static boolean isAsked=false;
    public static boolean askedSpecific=false;
    public static boolean intentIsRecommend=false;
    public static boolean intentIsAvailable=false;
    public static boolean intentIsPrice=false;
    public static boolean intentIsAddToCart=false;
    public static boolean isaddToCartAPI=false;


    //Intent specific Info
    public static String contextId;
    public static String lastIntent="";

    //Layout Components
    public static RecyclerView mRecyclerView;
    public static GoogleBot_ChatAdapter mAdapter;
    public static EditText chatText;
    public static ImageButton sendButton;
    public static RecyclerView.LayoutManager mLayoutManager;

    public static List<String> mDataset;
    public static ArrayList<Integer> mDatasetTypes;

    public static List<String> entityItemList =new ArrayList<>();
    public static List<String> entityBooleanList =new ArrayList<>();
    public static List<Integer> entityNumberList =new ArrayList<>();
    public static List<String> itemMatchedList =new ArrayList<>();
    public static List<String> itemMatchedIdList = new ArrayList<>();

    public static List<GoogleBot_ChatDataModel> chatDataModels;

    public static int countPrompt=0;



}
