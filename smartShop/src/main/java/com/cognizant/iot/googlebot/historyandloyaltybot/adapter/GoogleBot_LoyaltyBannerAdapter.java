package com.cognizant.iot.googlebot.historyandloyaltybot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import com.cognizant.iot.googlebot.historyandloyaltybot.model.GoogleBot_LoyaltyBanner;
import com.cognizant.retailmate.R;

import java.util.ArrayList;

/**
 * Created by Guest_User on 25/01/17.
 */

public class GoogleBot_LoyaltyBannerAdapter extends RecyclerView.Adapter<GoogleBot_LoyaltyBannerAdapter.LoyaltyViewHolder> {
    Context context;
    ArrayList<GoogleBot_LoyaltyBanner> loyaltyBannerArrayList;

    public GoogleBot_LoyaltyBannerAdapter(Context context, ArrayList<GoogleBot_LoyaltyBanner> loyaltyBannerArrayList) {
        this.context = context;
        this.loyaltyBannerArrayList = loyaltyBannerArrayList;
    }

    @Override
    public LoyaltyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.googlechatbot_loyalty_banner,parent,false);
        LoyaltyViewHolder lv=new LoyaltyViewHolder(view);
        return lv;
    }

    @Override
    public void onBindViewHolder(LoyaltyViewHolder holder, int position) {
        final GoogleBot_LoyaltyBanner loyaltyBanner=loyaltyBannerArrayList.get(position);
        holder.loyaltyimage.setImageResource(R.drawable.reward_icon);
        holder.loyaltypoints.setText(loyaltyBanner.getLoyaltypoints());
    }

    @Override
    public int getItemCount() {
        return loyaltyBannerArrayList.size();
    }

    public class LoyaltyViewHolder extends RecyclerView.ViewHolder{
        ImageView loyaltyimage;
        TextView loyaltypoints;
        public LoyaltyViewHolder(View itemView) {
            super(itemView);
            loyaltyimage= (ImageView) itemView.findViewById(R.id.loyalty_icon);
            loyaltypoints= (TextView) itemView.findViewById(R.id.loyaltypoints);
        }
    }
}
