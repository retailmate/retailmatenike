package com.cognizant.iot.googlebot.historyandloyaltybot.model;

/**
 * Created by Guest_User on 11/01/17.
 */

public class GoogleBot_RecommendModel {
    private String url;
    private String productname;
    private String amount;


    public String getImageurl() {
        return url;
    }

    public void setImageurl(String url) {
        this.url = url;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
