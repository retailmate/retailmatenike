package com.cognizant.iot.activityfindproduct;

public class ProductData_findprod {
	String id, name, desc, itemid, image_url, price, category, offer, unit,
			lastdate, flag, allergy;

	public ProductData_findprod(String id, String name, String price,
			String desc, String imageurl, String category, String offer,
			String lastdate, String unit, String flag, String itemid) {
		this.setId(id);
		this.setName(name);
		this.setDesc(desc);
		this.setImageUrl(imageurl);
		this.setPrice(price);
		this.setCategory(category);
		this.setLastdate(lastdate);
		this.setUnit(unit);
		this.setOffer(offer);
		this.setFlag(flag);
		this.setitemID(itemid);
		// this.setAllergy(allergy);

	}

	public String getitemID() {
		return itemid;
	}

	public void setitemID(String itemid) {
		this.itemid = itemid;
	}

	public String getAllergy() {
		return allergy;
	}

	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}

	public void setImageUrl(String imageurl) {
		this.image_url = imageurl;
	}

	public String getImageUrl() {
		return image_url;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getPrice() {
		return price;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory() {
		return category;
	}

	public void setOffer(String offer) {
		this.offer = offer;
	}

	public String getOffer() {
		return offer;
	}

	public void setLastdate(String lastdate) {
		this.lastdate = lastdate;
	}

	public String getLastdate() {
		return lastdate;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getUnit() {
		return unit;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getFlag() {
		return flag;
	}
}
