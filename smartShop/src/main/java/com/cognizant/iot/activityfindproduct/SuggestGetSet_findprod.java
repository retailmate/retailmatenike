package com.cognizant.iot.activityfindproduct;

public class SuggestGetSet_findprod 
{
	 
    String id,name,desc;
    
    public SuggestGetSet_findprod(String id, String name, String desc)
    {
        this.setId(id);
        this.setName(name);
        this.setDesc(desc);
    }
    public String getId() 
    {
        return id;
    }
 
    public void setId(String id) 
    {
        this.id = id;
    }
 
    public String getName() 
    {
        return name;
    }
 
    public void setName(String name) 
    {
        this.name = name;
    }
    
    public void setDesc(String desc) 
    {
        this.desc = desc;
    }
    
    public String getDesc() 
    {
        return desc;
    } 
}