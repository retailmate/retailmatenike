package com.cognizant.iot.wearable.companion;

/**
 * Created by carlos on 7/16/14.
 */
public class Constants {
    public static final String PATH_DISMISS = "/dismissnotification";
    public static final String PATH_SERVER_RESPONSE = "/response";
    public static final String PATH_SERVER_REQUEST = "/request";
    public static final String PATH_NOTIFICATION = "/notification";
    public static final String KEY_TITLE = "title";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_TIMESTAMP = "timestamp";
}
