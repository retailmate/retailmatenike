package com.cognizant.iot.bopis;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cognizant.retailmate.R;

import java.util.List;

/**
 * Created by 452781 on 11/9/2016.
 */
public class RmStoreAdapter extends RecyclerView.Adapter<RmStoreAdapter.MyViewHolder> {

    private List<RmStoreModel> storeModelList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, address;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.store_name);
            address = (TextView) view.findViewById(R.id.store_address);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//
////        // put the message in Intent
//        intent.putExtra("MESSAGE","");
////        // Set The Result in Intent
//        setResult(2,intent);
////        // finish The activity
//        finish();

                    Intent intent = new Intent(context, RmBopis.class);
                    intent.putExtra("storeLocation", name.getText());
                    ((Activity) v.getContext()).setResult(3, intent);
                    ((Activity) v.getContext()).finish();

                }
            });

        }
    }


    public RmStoreAdapter(List<RmStoreModel> storeModelList, Context context) {
        this.context = context;
        this.storeModelList = storeModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bopis_rm_store_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RmStoreModel rmStoreModel = storeModelList.get(position);
        holder.name.setText(rmStoreModel.getName());
        holder.address.setText(rmStoreModel.getAddress());

    }

    @Override
    public int getItemCount() {
        return storeModelList.size();
    }
}