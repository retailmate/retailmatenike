package com.cognizant.iot.wishlist.activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.retailmate.R;

public class Share_Wishlist extends Activity {

	EditText emailtxt;
	Button share;
	static String imei = "000000000000789";
	JSONObject jsonobject;
	JSONArray jsonarray;
	ListView listview;
	String email;
	String message;

	ProgressDialog mProgressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_wishlist_layout);

		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayShowTitleEnabled(true);
		getActionBar().setBackgroundDrawable(
				new ColorDrawable(Color.parseColor("#0577D9")));
		getActionBar().setIcon(
				new ColorDrawable(getResources().getColor(
						android.R.color.transparent)));

		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");

		TextView abTitle = (TextView) findViewById(titleId);
		abTitle.setTextColor(Color.parseColor("#ffffff"));

		emailtxt = (EditText) findViewById(R.id.receiver_main1);
		share = (Button) findViewById(R.id.share_button);

		share.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				email = emailtxt.getText().toString();
				if (email.length() == 0)
					Toast.makeText(getApplicationContext(),
							"Please fill the email address first.",
							Toast.LENGTH_LONG).show();

				else
					new DownloadJSON().execute();
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// setMenuBackground();

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title

		// Handle action bar actions click
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// DownloadJSON AsyncTask
	private class DownloadJSON extends AsyncTask<Void, Void, Void> {
		JSONArray obj3;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(Share_Wishlist.this);
			// Set progressdialog title
			mProgressDialog.setTitle("Share Wishlist");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			/*
			 * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

			// Retrieve JSON Objects from the given URL address
			jsonobject = JSONfunctions_sharewishlist.getJSONfromURL(
					CatalogActivity.urlPart + "/api/WishList/ShareWishList",
					email);

			try {
				JSONObject obj2 = new JSONObject(jsonobject.getString("header"));
				message = obj2.getString("message");
				obj3 = (JSONArray) jsonobject.get("cusdetails");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void args) {
			// Toast.makeText(getApplicationContext(), jsonobject.toString(),
			// Toast.LENGTH_LONG).show();
			if (message.equals("Request processed successfully")) {
				if (obj3.length() == 0)
					Toast.makeText(
							getApplicationContext(),
							"The receiver's wishlist already has all the items.\nOr there is no such user with this email id.",
							Toast.LENGTH_LONG).show();
				else
					Toast.makeText(
							getApplicationContext(),
							"Success.!!!\n"
									+ obj3.length()
									+ " extra items added to receivers wishlist.",
							Toast.LENGTH_LONG).show();
			} else if (message.equals("Request failed successfully"))
				Toast.makeText(getApplicationContext(),
						"Please enter something in the text fields!",
						Toast.LENGTH_LONG).show();

			/*
			 * if(status.equals("Request processed successfully"))
			 * Toast.makeText(getApplicationContext(),
			 * "Success.!!!\nItems added to email is "+email,
			 * Toast.LENGTH_LONG).show();
			 */
			// Close the progressdialog
			mProgressDialog.dismiss();
		}
	}

}
