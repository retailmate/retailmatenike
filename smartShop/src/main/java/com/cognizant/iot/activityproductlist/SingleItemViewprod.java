package com.cognizant.iot.activityproductlist;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.retailmate.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import cz.msebera.android.httpclient.Header;

public class SingleItemViewprod extends Activity {
    // Declare Variables
    String prodid;
    String prodname;
    String prodprice;
    String flag;
    String position;
    ImageLoaderprod imageLoader = new ImageLoaderprod(this);
    ProgressDialog prgDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from singleitemview.xml
        setContentView(R.layout.singleitemviewprod);


        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);


        Intent i = getIntent();
        // Get the result of prodid
        prodid = i.getStringExtra("prodid");
        // Get the result of prodname
        prodname = i.getStringExtra("prodname");
        // Get the result of prodprice
        prodprice = i.getStringExtra("prodprice");
        // Get the result of flag
        flag = i.getStringExtra("flag");

        // Locate the TextViews in singleitemview.xml
        TextView txtprodid = (TextView) findViewById(R.id.prodid2);
        TextView txtprodname = (TextView) findViewById(R.id.prodname2);
        TextView txtprodprice = (TextView) findViewById(R.id.prodprice2);
        Button btnaddtowshlst = (Button) findViewById(R.id.addtowishlist_btn);


        // Locate the ImageView in singleitemview.xml
        ImageView imgflag = (ImageView) findViewById(R.id.flag2);

        // Set results to the TextViews
        txtprodid.setText(prodid);
        txtprodname.setText(prodname);
        txtprodprice.setText(prodprice);

        // Capture position and set results to the ImageView
        // Passes flag images URL into ImageLoader.class
        imageLoader.DisplayImage(flag, imgflag);


        btnaddtowshlst.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                RequestParams params = new RequestParams();
                params.put("macaddr", MainActivityprod.imei);
                params.put("wishname", "Marriage");
                params.put("products", prodid);

                addtowishlistService(params);

                Intent in = new Intent(SingleItemViewprod.this, MainActivityprod.class);
                startActivity(in);
            }
        });


    }


    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void addtowishlistService(RequestParams params) {

        //Toast.makeText(getApplicationContext(),"JSON request params is" + params.toString(),Toast.LENGTH_LONG).show();  		//to show the REQUEST

        // Show Progress Dialog
        prgDialog.show();
        // Make RESTful web service call using AsyncHttpClient object
    /*	System.setProperty("https.proxyHost", "10.243.115.76");
        System.setProperty("https.proxyPort", "6050");
		System.setProperty("http.proxyHost", "10.243.115.76");
		System.setProperty("http.proxyPort", "6050");*/

        AsyncHttpClient client = new AsyncHttpClient();

        client.post("http://c3a2351402de471d85e8257dd5d2f031.cloudapp.net/api/WishList/Add",
                params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {


                        // Hide Progress Dialog
                        prgDialog.hide();

                        try {
                            String response = new String(responseBody,"UTF-8");
                            // JSON Object
                            JSONObject obj = new JSONObject(response);
                            // When the JSON response has status boolean value
                            // assigned with true


                            //Toast.makeText(getApplicationContext(),"The JSON IS \n" +obj.toString(),Toast.LENGTH_LONG).show();  //to show the RESPONSE

                            JSONObject obj1 = new JSONObject(obj.getString("header"));

                            String msg = obj1.getString("message");
                            //Toast.makeText(getApplicationContext(),"Response is\n" +obj1.toString(),Toast.LENGTH_LONG).show();


                            if (msg.equalsIgnoreCase("Request processed successfully")) {

                                // Set Default Values for Edit View controls
                                //setDefaultValues();

                                //	Toast.makeText(getApplicationContext(),"Request processed successfully",Toast.LENGTH_LONG).show();

                                String status = obj1.getString("status");

                                String str4 = null, str3 = null, str5 = null;
                                if (status.equalsIgnoreCase("success")) {

                                    JSONArray obj3 = (JSONArray) obj.get("cusdetails");


                                    try {
                                        str3 = (String) obj3.getJSONObject(0).get("customer");
                                        str4 = (String) obj3.getJSONObject(0).get("Products added");
                                        Toast.makeText(getApplicationContext(), "Request processed successfully\nProduct " + str4 + " added to wishlist for Customer: " + str3, Toast.LENGTH_LONG).show();
                                    } catch (JSONException j) {

                                        str5 = (String) obj3.getJSONObject(0).get("product");
                                        Toast.makeText(getApplicationContext(), "Product " + str5 + " in the wishlist", Toast.LENGTH_LONG).show();
                                    }
                                }


								/*JSONArray obj3=(JSONArray) obj1.get("cusdetails");
								String str=null;
								for(int i=0;i<obj3.length();i++)
								{
									str=(String) obj3.getJSONObject(i).get("Authenticate");
									// Toast.makeText(getApplicationContext(),"customer id is :  " +str,Toast.LENGTH_LONG).show();
										-
								}
								if(str.equals("User password matches"))
								{

								Toast.makeText(getApplicationContext(),
										"You are successfully Logged in Successfully!",
										Toast.LENGTH_LONG).show();



								}


							else if (str.equalsIgnoreCase("Email given does not match"))
							{

								Toast.makeText(getApplicationContext(),
										"Login Failed !!!\nPlease provide the correct username with which you registered on this application.",
										Toast.LENGTH_LONG).show();

							}

							else if (str.equalsIgnoreCase("User password does not match"))
							{
								Toast.makeText(getApplicationContext(),
										"User password doesnot match",
										Toast.LENGTH_LONG).show();
							}

							}*/

                            }
                        } catch (IOException io) {
                            io.printStackTrace();
                        } catch (JSONException e) {

                            // TODO Auto-generated catch block
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Error Occured [Server's JSON response might be invalid]!",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();

                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        // Hide Progress Dialog
                        prgDialog.hide();
                        // When Http response code is '404'

						/*
						 * // JSON Object try { JSONObject obj = new
						 * JSONObject(content); obj.get("ErrorResponse");
						 *
						 *
						 * } catch (JSONException e) { // TODO Auto-generated
						 * catch block e.printStackTrace(); }
						 */

                        if (statusCode == 404) {
                            Toast.makeText(getApplicationContext(),
                                    "Requested resource not found",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Toast.makeText(getApplicationContext(),
                                    "Something went wrong at server end",
                                    Toast.LENGTH_LONG).show();
                        }
                        // When Http response code other than 404, 500
                        else {
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]",
                                    Toast.LENGTH_LONG).show();
                        }
                    }

                });
    }

}