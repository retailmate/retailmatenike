package com.cognizant.iot.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.iot.activitywishlist.ImageLoader_wish;
import com.cognizant.iot.activitywishlist.JSONfunctions_wish_deleteproduct;
import com.cognizant.iot.activitywishlist.SingleItemView_wish;
import com.cognizant.retailmate.R;

public class ListViewAdapter_wish_full extends BaseAdapter {

	// Declare Variables
	Context context;
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	ImageLoader_wish imageLoader;
	HashMap<String, String> resultp = new HashMap<String, String>();
	ProgressDialog mProgressDialog;
	JSONObject jsonobject;
	JSONArray jsonarray;

	ArrayList<HashMap<String, String>> arraylist;
	String prodid1;

	public ListViewAdapter_wish_full(Context context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		data = arraylist;
		imageLoader = new ImageLoader_wish(context);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables
		TextView category;
		TextView unit;
		TextView prodname;
		TextView prodprice, offer, offer_in_tag;
		ImageView flag;
		ImageButton delete_img_btn;
		ImageView tag;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View itemView = inflater.inflate(R.layout.listview_item_wish_vertical,
				parent, false);
		// Get the position
		resultp = data.get(position);

		// Locate the TextViews in listview_item.xml
		category = (TextView) itemView.findViewById(R.id.prodcategory_wish);
		prodname = (TextView) itemView.findViewById(R.id.prodname_wish);
		prodprice = (TextView) itemView.findViewById(R.id.prodprice_wish1);
		delete_img_btn = (ImageButton) itemView
				.findViewById(R.id.delete_small_trash1);
		// Locate the ImageView in listview_item.xml
		flag = (ImageView) itemView.findViewById(R.id.flag_wish);
		offer = (TextView) itemView.findViewById(R.id.offers_wish);
		offer_in_tag = (TextView) itemView.findViewById(R.id.offer_in_tag);
		unit = (TextView) itemView.findViewById(R.id.unit);
		tag = (ImageView) itemView.findViewById(R.id.tag);

		// Capture position and set results to the TextViews
		category.setText(resultp.get(CatalogActivity.CATEGORY));
		prodname.setText(resultp.get(CatalogActivity.PRODNAME));
		prodprice.setText(resultp.get(CatalogActivity.PRODPRICE));
		unit.setText(resultp.get(CatalogActivity.UNIT));

		if (resultp.get(CatalogActivity.OFFER).length() > 0) {
			offer.setTextSize(15);
			offer.setText(resultp.get(CatalogActivity.OFFER));
			offer_in_tag.setText(resultp.get(CatalogActivity.OFFER));
		}

		else {

			tag.setVisibility(View.INVISIBLE);
			offer_in_tag.setVisibility(View.INVISIBLE);
			offer.setTextColor(Color.DKGRAY);
			offer.setText("No offers for this product");
		}

		// Capture position and set results to the ImageView
		// Passes flag images URL into ImageLoader.class
		imageLoader.DisplayImage(resultp.get(CatalogActivity.FLAG), flag);
		// Capture ListView item click

		/*
		 * delete_img_btn.setBackgroundColor(Color.RED);
		 */
		delete_img_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				resultp = data.get(position);
				prodid1 = resultp.get(CatalogActivity.PRODID);
				// TODO Auto-generated method stub
				// Toast.makeText(context,
				// "the prodid is "+resultp.get(Wishlist_Screen.PRODID),
				// Toast.LENGTH_LONG).show();
				new DownloadJSONfordelete().execute();
				/*
				 * Intent hh=new Intent(context,Wishlist_Screen.class);
				 * hh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				 * context.startActivity(hh);
				 */

			}
		});

		itemView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Get the position
				resultp = data.get(position);
				Intent intent = new Intent(context, SingleItemView_wish.class);
				// Pass all data prodid
				intent.putExtra("prodid", resultp.get(CatalogActivity.PRODID));
				// Pass all data prodname
				intent.putExtra("prodname",
						resultp.get(CatalogActivity.PRODNAME));
				// Pass all data prodprice
				intent.putExtra("prodprice",
						resultp.get(CatalogActivity.PRODPRICE));
				// Pass all data flag
				intent.putExtra("flag", resultp.get(CatalogActivity.FLAG));
				intent.putExtra("desc", resultp.get(CatalogActivity.DESC));
				intent.putExtra("beacon", resultp.get(CatalogActivity.BEACON));
				// Start SingleItemView Class
				context.startActivity(intent);

			}
		});
		return itemView;
	}

	// DownloadJSON AsyncTask
	private class DownloadJSONfordelete extends AsyncTask<Void, Void, Void> {
		String str4 = null, str3 = null, str5 = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// Create a progressdialog
			mProgressDialog = new ProgressDialog(context);
			// Set progressdialog title
			mProgressDialog.setTitle("Deleting");
			// Set progressdialog message
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			// Show progressdialog
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			/*
			 * System.setProperty("https.proxyHost", "10.243.115.76");
			 * System.setProperty("https.proxyPort", "6050");
			 * System.setProperty("http.proxyHost", "10.243.115.76");
			 * System.setProperty("http.proxyPort", "6050");
			 */

			// Create an array
			arraylist = new ArrayList<HashMap<String, String>>();
			// Retrieve JSON Objects from the given URL address
			jsonobject = JSONfunctions_wish_deleteproduct.getJSONfromURL(
					CatalogActivity.urlPart
							+ "/api/WishList/DeleteWishlistProducts", prodid1);

			try {

				JSONObject obj1 = new JSONObject(jsonobject.getString("header"));

				String message = obj1.getString("message");
				// Toast.makeText(getApplicationContext(),"Response is\n"
				// +obj1.toString(),Toast.LENGTH_LONG).show();

				if (message.equalsIgnoreCase("Request processed successfully")) {

					// Toast.makeText(getApplicationContext(),"Request processed successfully",Toast.LENGTH_LONG).show();

					String status = obj1.getString("status");

					if (status.equalsIgnoreCase("success")) {

						JSONArray obj3 = (JSONArray) jsonobject
								.get("cusdetails");

						try {
							str3 = (String) obj3.getJSONObject(0).get("prodid");
							// str4=(String)
							// obj3.getJSONObject(0).get("Products added");
							/*
							 * Toast.makeText(getApplicationContext(),
							 * "Request processed successfully\nProduct "
							 * +str3,Toast.LENGTH_LONG).show();
							 */

						}

						catch (JSONException j) {

							j.printStackTrace();
							/*
							 * Toast.makeText(getApplicationContext(),
							 * "Some error in reading JSON"
							 * ,Toast.LENGTH_LONG).show();
							 */
						}
					}

				}
			} catch (JSONException e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void args) {

			// Toast.makeText(getApplicationContext(),jsonobject.toString() ,
			// Toast.LENGTH_LONG).show();
			// Intent i=new
			// Intent(SingleItemView_wish.class,MainActivity_wish.class);
			mProgressDialog.dismiss();
			Toast.makeText(context,
					"Request processed successfully\nProduct " + str3,
					Toast.LENGTH_LONG).show();
			Intent hh = new Intent(context, CatalogActivity.class);
			hh.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			context.startActivity(hh);

		}
	}
}