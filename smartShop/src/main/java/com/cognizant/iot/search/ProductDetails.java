package com.cognizant.iot.search;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.iot.activity.CatalogActivity;
import com.cognizant.retailmate.R;

public class ProductDetails extends Activity {
	public String value = "";
	Intent myIntent = getIntent();
	JSONObject jsonobject;
	JSONArray jsonarray;
	String imageUrl;
	TextView product_id, product_name, product_description, product_price;
	ImageView imageViewObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_details);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			value = extras.getString("ProductId");
			Toast.makeText(getApplicationContext(), value, Toast.LENGTH_SHORT)
					.show();
		}
		product_id = (TextView) findViewById(R.id.textView_pid);
		product_name = (TextView) findViewById(R.id.textView_name);
		product_description = (TextView) findViewById(R.id.textView_des);
		product_price = (TextView) findViewById(R.id.textView_price);
		imageViewObject = (ImageView) findViewById(R.id.imageView_pimage);
		new AdapterUpdaterTask().execute();

	}

	private class AdapterUpdaterTask extends AsyncTask<Void, Void, Void> {
		String prodid, name;
		List<ProductData> ListData = new ArrayList<ProductData>();

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected Void doInBackground(Void... params) {
			Log.i("UPDATE", "1");
			// System.setProperty("https.proxyHost", "10.243.115.76");
			// System.setProperty("https.proxyPort", "6050");
			// System.setProperty("http.proxyHost", "10.243.115.76");
			// System.setProperty("http.proxyPort", "6050");
			JSONObject jsonobject1 = JSONFunctionsDetails.getJSONfromURL(
					CatalogActivity.urlPart + "/api/Product/GetProductDetails",
					value);

			try {
				jsonobject = new JSONObject(
						jsonobject1.getString("ProductResponse"));
				jsonarray = jsonobject.getJSONArray("assets");

				for (int i = 0; i < jsonarray.length(); i++) {
					jsonobject = jsonarray.getJSONObject(i);
					ListData.add(new ProductData(jsonobject
							.getString("ProductID"), jsonobject
							.getString("Name"), jsonobject.getString("Cost"),
							jsonobject.getString("Desc"), jsonobject
									.getString("thumbnail")));
				}

			} catch (JSONException e) {

				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			Log.i("UPDATE", "2");
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {

			int size = ListData.size();
			if (size > 0) {
				// Log.i("ADAPTER_SIZE", "" + size);
				for (int i = 0; i < ListData.size(); i++) {

					product_id.setText(ListData.get(i).getId());
					product_name.setText(ListData.get(i).getName());
					product_description.setText(ListData.get(i).getDesc());
					product_price.setText(ListData.get(i).getPrice());
					imageUrl = ListData.get(i).getImageUrl();
					new DownloadImageTask(
							(ImageView) findViewById(R.id.imageView_pimage))
							.execute(imageUrl);
				}
				Log.i("UPDATE", "4");
			} else {
				Toast.makeText(getApplicationContext(), "No detail found",
						Toast.LENGTH_SHORT).show();
			}
			super.onPostExecute(aVoid);
		}
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}
	}
}
