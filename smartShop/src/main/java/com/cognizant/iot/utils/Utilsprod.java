package com.cognizant.iot.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Utilsprod {
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }

    public static boolean[] convertBoolean(List<Boolean> booleans)
    {
        boolean[] ret = new boolean[booleans.size()];
        Iterator<Boolean> iterator = booleans.iterator();
        for (int i = 0; i < ret.length; i++)
        {
            ret[i] = iterator.next().booleanValue();
        }
        return ret;
    }

    public static JSONArray getSelectedCategoriesInString(List<HashMap<String, Object>> categoriesList){
        JSONArray selectedCategoryArray = new JSONArray();
        try {
            for(int i=0; i<categoriesList.size();i++ ){
                HashMap<String, Object> category = categoriesList.get(i);
                if(category.get("IsSelected") != null && (boolean) category.get("IsSelected")){
                    JSONObject categoryObject = new JSONObject();

                    if(category.get("CategoryHierarchy") != null){
                        categoryObject.accumulate("categoryhierarchy", (String) category.get("CategoryHierarchy"));
                    }

                    if(category.get("Category") != null){
                        categoryObject.accumulate("category", (String) category.get("Category"));
                    }

                    if(category.get("HasKnowledge") != null && (boolean) category.get("HasKnowledge")){
                        if(category.get("KnowledgeLevel") != null){
                            categoryObject.accumulate("knowledge", (int) category.get("KnowledgeLevel"));
                        }
                    }
                    selectedCategoryArray.put(categoryObject);
                }
            }

        } catch (JSONException je) {
            je.printStackTrace();
        }

        return selectedCategoryArray;
    }

    public static JSONArray getFavColorString(String value){
        JSONArray selectedColorArray = new JSONArray();

        String[] colorsArray = value.split(",");
        for(int i=0; i<colorsArray.length; i++){
            selectedColorArray.put(colorsArray[i]);
        }
        return selectedColorArray;
    }
}
