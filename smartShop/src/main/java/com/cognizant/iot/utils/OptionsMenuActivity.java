package com.cognizant.iot.utils;

import java.lang.reflect.Field;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.PopupMenu;

import com.cognizant.iot.activity.Find_Product;
import com.cognizant.iot.json.AssetsExtracter;
import com.cognizant.iot.json.Logout;
import com.cognizant.retailmate.R;

public class OptionsMenuActivity extends Activity {
	Intent wishlist_intent;
	AssetsExtracter mTask;
	com.cognizant.iot.json.Logout mTasklogout;
	// String imei = "000000000000789";

	public String imei;

	private static final String TAG = OptionsMenuActivity.class.getSimpleName();

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case android.R.id.home:
			onBackPressed();
			return true;

		case R.id.options:

			PopupMenu popup = new PopupMenu(OptionsMenuActivity.this,
					findViewById(R.id.options));
			// Inflating the Popup using xml file
			popup.getMenuInflater().inflate(R.menu.main_old, popup.getMenu());
			popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

				@Override
				public boolean onMenuItemClick(MenuItem item) {
					// TODO Auto-generated method stub
					// TODO Auto-generated method stub
					switch (item.getItemId()) {
					case R.id.my_wishlist:
						wishlist_intent = new Intent(
								OptionsMenuActivity.this,
								com.cognizant.iot.wishlist.activity.Wishlist_Screen.class);

						wishlist_intent.putExtra("imeinmbr", imei);
						startActivity(wishlist_intent);

						break;

					case R.id.find_store:
						wishlist_intent = new Intent(
								OptionsMenuActivity.this,
								com.cognizant.iot.googlemap.GoogleMapInitial.class);
						startActivity(wishlist_intent);
						break;

					case R.id.find_product:
						wishlist_intent = new Intent(OptionsMenuActivity.this,
								Find_Product.class);
						wishlist_intent.putExtra("imeinmbr", imei);
						startActivity(wishlist_intent);

						break;

					case R.id.mobile_offer:
						mTask = new AssetsExtracter("empty",
								OptionsMenuActivity.this);
						mTask.execute();

						break;

					case R.id.my_order:
						wishlist_intent = new Intent(
								OptionsMenuActivity.this,
								com.cognizant.iot.orderhistory.activity.Order_Screen.class);
						wishlist_intent.putExtra("imeinmbr", imei);
						startActivity(wishlist_intent);
						break;
					case R.id.logout_option:
						// wishlist_intent = new
						// Intent(OptionsMenuActivity.this,
						// Logout_Activity.class);
						// startActivity(wishlist_intent);
						mTasklogout = new Logout("empty",
								OptionsMenuActivity.this, imei);
						System.out.println("LOGOUT IMEI" + imei);
						mTasklogout.execute();
						// finish();
						break;
					}
					return true;

				}

			});

			Object menuHelper;
			Class[] argTypes;
			try {
				Field fMenuHelper = PopupMenu.class.getDeclaredField("mPopup");
				fMenuHelper.setAccessible(true);
				menuHelper = fMenuHelper.get(popup);
				argTypes = new Class[] { boolean.class };
				menuHelper.getClass()
						.getDeclaredMethod("setForceShowIcon", argTypes)
						.invoke(menuHelper, true);
			} catch (Exception e) {

				Log.w(TAG, "error forcing menu icons to show", e);
				popup.show();
				return true;
			}
			/*
			 * END
			 */

			popup.show();
		}
		return super.onOptionsItemSelected(item);
	}
}