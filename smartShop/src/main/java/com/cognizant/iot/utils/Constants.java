package com.cognizant.iot.utils;

/**
 * Created by 429023 on 8/11/2016.
 */
public class Constants {
    public static String REGISTRATION_REQUEST = "registration_request";
    public static String REGISTRATION_CATEGORY_REQUEST = "registration_category_request";

    public static String REGISTRATION_ENDPOINT = "http://nikecustomervalidationax.azurewebsites.net/api/Customer/CustomerList?idToken=";
    public static String REGISTRATION_CATEGORY_ENDPOINT = "http://rmnikeapiapp.azurewebsites.net/api/CustomerRegistration/CustomerRegistrationAPI";
    public static String CART_ADD_ITEM_REQUEST = "http://nikecustomervalidationax.azurewebsites.net/api/cart/CreateCart?idToken=";
    public static String CART_LIST_ITEM_REQUEST = "http://nikecustomervalidationax.azurewebsites.net/api/cart/GetCart?idToken=";
    public static String GET_PRODUCT_BY_IDS = "https://Nikedevret.cloudax.dynamics.com/Commerce/Products/SearchByText(channelId=0,catalogId=0,searchText='%s')?$top=20&api-version=7.1";

    public static String CREATE_PURCHASE_ORDER = "http://rmnikeapiapp.azurewebsites.net/api/JDAServices/CreateCustomerOrderAPI";
}
