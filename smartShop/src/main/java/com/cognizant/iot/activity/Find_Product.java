package com.cognizant.iot.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.cognizant.iot.utils.OptionsMenuActivity;
import com.cognizant.retailmate.R;

public class Find_Product extends OptionsMenuActivity {
	Intent intentObject;
	public String product_name = "hello";

	private static final String[] COUNTRIES = new String[] { "Shoes", "Shirts",
			"Trousers", "Home Appliances", "Wathches", "Electronics" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.find_product);

		final AutoCompleteTextView auto = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, COUNTRIES);
		auto.setAdapter(adapter);

		Button bt = (Button) findViewById(R.id.search_product_btn);

		bt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				product_name = auto.getEditableText().toString();

				Toast.makeText(getApplicationContext(), product_name,
						Toast.LENGTH_SHORT).show();

				intentObject = new Intent(getApplicationContext(),
						Product_details_FindProduct.class);

				intentObject.putExtra("ProductName", product_name);

				startActivity(intentObject);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_old, menu);
		return true;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		finish();
		super.onPause();
	}
}
