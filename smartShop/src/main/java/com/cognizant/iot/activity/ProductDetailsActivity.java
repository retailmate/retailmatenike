package com.cognizant.iot.activity;

import java.util.List;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.iot.json.AssetsExtracter;
import com.cognizant.iot.utils.OptionsMenuActivity;
import com.cognizant.retailmate.R;

public class ProductDetailsActivity extends OptionsMenuActivity {
	AssetsExtracter mTask;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.productdetails);

		List<Product> catalog = ShoppingWishlistHelper
				.getCatalog(getResources());
		final List<Product> cart = ShoppingWishlistHelper.getCart();

		// to extract the extra information out of the Intent
		int productIndex = getIntent().getExtras().getInt(
				ShoppingWishlistHelper.PRODUCT_INDEX);
		final Product selectedProduct = catalog.get(productIndex);

		// Set the proper image and text
		ImageView productImageView = (ImageView) findViewById(R.id.ImageViewProduct);
		productImageView.setImageDrawable(selectedProduct.productImage);

		TextView productTitleTextView = (TextView) findViewById(R.id.TextViewProductTitle);
		productTitleTextView.setText(selectedProduct.title);

		TextView productDetailsTextView = (TextView) findViewById(R.id.TextViewProductDetails);
		productDetailsTextView.setText(selectedProduct.description);

		TextView productPriceTextView = (TextView) findViewById(R.id.TextViewProductPrice);
		productPriceTextView.setText("Rs." + selectedProduct.price);

		Button addToCartButton = (Button) findViewById(R.id.ButtonAddToCart);
		addToCartButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				cart.add(selectedProduct);
				finish();
			}
		});

		// Disable the add to cart button if the item is already in the cart
		if (cart.contains(selectedProduct)) {
			addToCartButton.setEnabled(false);
			addToCartButton.setText("Item Already in List");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_old, menu);
		return true;
	}

}
