package com.cognizant.iot.model;

/**
 * Created by 452781 on 12/6/2016.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OneRetailWishlistCheckModel {
    @SerializedName("CUST_ID")
    @Expose
    private String cUSTID;
    @SerializedName("CUST_NAME")
    @Expose
    private String cUSTNAME;
    @SerializedName("GENDER")
    @Expose
    private String gENDER;
    @SerializedName("AGE")
    @Expose
    private String aGE;
    @SerializedName("LOCATION")
    @Expose
    private String lOCATION;
    @SerializedName("PRIMARY_INTEREST")
    @Expose
    private String pRIMARYINTEREST;
    @SerializedName("IMG_URL")
    @Expose
    private String iMGURL;
    @SerializedName("OOS_SKU")
    @Expose
    private String oOSSKU;
    @SerializedName("OOS_REC_SKU")
    @Expose
    private String oOSRECSKU;

    /**
     * @return The cUSTID
     */
    public String getCUSTID() {
        return cUSTID;
    }

    /**
     * @param cUSTID The CUST_ID
     */
    public void setCUSTID(String cUSTID) {
        this.cUSTID = cUSTID;
    }

    /**
     * @return The cUSTNAME
     */
    public String getCUSTNAME() {
        return cUSTNAME;
    }

    /**
     * @param cUSTNAME The CUST_NAME
     */
    public void setCUSTNAME(String cUSTNAME) {
        this.cUSTNAME = cUSTNAME;
    }

    /**
     * @return The gENDER
     */
    public String getGENDER() {
        return gENDER;
    }

    /**
     * @param gENDER The GENDER
     */
    public void setGENDER(String gENDER) {
        this.gENDER = gENDER;
    }

    /**
     * @return The aGE
     */
    public String getAGE() {
        return aGE;
    }

    /**
     * @param aGE The AGE
     */
    public void setAGE(String aGE) {
        this.aGE = aGE;
    }

    /**
     * @return The lOCATION
     */
    public String getLOCATION() {
        return lOCATION;
    }

    /**
     * @param lOCATION The LOCATION
     */
    public void setLOCATION(String lOCATION) {
        this.lOCATION = lOCATION;
    }

    /**
     * @return The pRIMARYINTEREST
     */
    public String getPRIMARYINTEREST() {
        return pRIMARYINTEREST;
    }

    /**
     * @param pRIMARYINTEREST The PRIMARY_INTEREST
     */
    public void setPRIMARYINTEREST(String pRIMARYINTEREST) {
        this.pRIMARYINTEREST = pRIMARYINTEREST;
    }

    /**
     * @return The iMGURL
     */
    public String getIMGURL() {
        return iMGURL;
    }

    /**
     * @param iMGURL The IMG_URL
     */
    public void setIMGURL(String iMGURL) {
        this.iMGURL = iMGURL;
    }

    /**
     * @return The oOSSKU
     */
    public String getOOSSKU() {
        return oOSSKU;
    }

    /**
     * @param oOSSKU The OOS_SKU
     */
    public void setOOSSKU(String oOSSKU) {
        this.oOSSKU = oOSSKU;
    }

    /**
     * @return The oOSRECSKU
     */
    public String getOOSRECSKU() {
        return oOSRECSKU;
    }

    /**
     * @param oOSRECSKU The OOS_REC_SKU
     */
    public void setOOSRECSKU(String oOSRECSKU) {
        this.oOSRECSKU = oOSRECSKU;
    }

}