{
	"@odata.context": "https://cts-ax7rtwupd1testdevaos.cloudax.dynamics.com/data/$metadata#ProductColors",
	"value": [{
		"@odata.etag": "W/\"JzAsMjI1NjU0MjA5MzQn\"",
		"ColorId": "White"
	}, {
		"@odata.etag": "W/\"JzAsMjI1NjU0MjExODMn\"",
		"ColorId": "Brown"
	}, {
		"@odata.etag": "W/\"JzAsMjI1NjU0MjExODYn\"",
		"ColorId": "Blue"
	}, {
		"@odata.etag": "W/\"JzAsMjI1NjU0MjExOTAn\"",
		"ColorId": "Red"
	}, {
		"@odata.etag": "W/\"JzAsMjI1NjU0MjExOTEn\"",
		"ColorId": "Yellow"
	}]
}